# How to install
```
$ poetry install
```

# How to fetch the data
Note it needs the token itself, not the file.

```
$ env CONDUIT_API_TOKEN=<conduit-token> poetry run python -m tasks.fetch_and_store
$ env CONDUIT_API_TOKEN=<conduit-token> poetry run python -m diffs.fetch_and_store
```

For an optional later start:

```
$ env CONDUIT_API_TOKEN=<conduit-token> poetry run python -m tasks.fetch_and_store "2020-02-01"
```

# How to use the data
To plot the number of new tasks over time:

```
poetry run python -m tasks.plot_days new --start 2020-08-01 --end 2020-08-31
```

To plot the number of fixed commits over time:
```
poetry run python -m tasks.plot_days committedfix --git-repository=~/src/blender/blender --start 2020-01-01
```

To plot the number of new patches over time:

```
poetry run python -m diffs.plot_days new --start 2020-08-01 --end 2020-08-31
```

To save the values of the data:
```
poetry run python -m tasks.update-plotted-db ~/src/blender/blender
poetry run python -m diffs.update-plotted-db
```

To calculate the response time:
```
poetry run python -m tasks.response_time website --start 2019-01-01 --end 2020-01-01
```
