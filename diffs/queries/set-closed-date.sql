WITH INPUT AS (SELECT
	:DiffId AS diff_phid
	--"PHID-DREV-kitxckogb6gohcn7wf4a" AS diff_phid
	--"PHID-DREV-delyydp5hcgil6f5qxg6"	AS diff_phid
)
UPDATE
	diffs
SET
	date_closed =
(	
SELECT
	--diffs.id,
	--diffs.status,
	CASE
		WHEN diffs.status NOT in (
			"abandoned",
			"published"
		)
		THEN NULL -- still open
	ELSE
(
	SELECT
		last_close_event.date
	FROM
	(
		SELECT
			t.date_created AS date,
			1 AS _order
		FROM transactions_close ts
			LEFT JOIN transactions t ON ts.transaction_id = t.id
			WHERE t.diff_phid = diffs.phid
		UNION ALL
		SELECT
			t.date_created as date,
			1 AS _order
		FROM transactions_abandon ta
			LEFT JOIN transactions t ON ta.transaction_id = t.id
			WHERE t.diff_phid = diffs.phid
		UNION ALL
		SELECT
			t.date_created as date,
			1 AS _order
		FROM transactions_action ta
			LEFT JOIN transactions t ON ta.transaction_id = t.id
			WHERE
				t.diff_phid = diffs.phid AND
				ta.new in (
					'abandon',
					--'accept',
					--'claim',
					'commit',
					--'reclaim',
					--'reject',
					'reopen'
					--'request_review',
					--'resign',
					--'rethink',
					)
		UNION ALL
		SELECT
			diffs.date_modified as date,
			2 AS _order
		ORDER BY
				_order,
				date DESC
			LIMIT 1
	) AS last_close_event
)
END AS date_closed

FROM
	diffs,
	INPUT
WHERE
	diffs.phid = INPUT.diff_phid
)
FROM
    INPUT
WHERE
    diffs.phid = INPUT.diff_phid
;