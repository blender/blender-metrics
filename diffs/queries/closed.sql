SELECT count(*) FROM diffs
WHERE
    diffs.date_closed is not NULL
    AND diffs.date_closed >= :Date
    AND diffs.date_closed < date(:Date, '+1 day')
    AND (
        (:StatusAbandoned AND :StatusPublished) OR
        (:StatusAbandoned AND diffs.status = 'abandoned') OR
        (:StatusPublished AND diffs.status = 'published')
    )
    AND
        ((NOT :IsCommunity) OR
            diffs.author_phid NOT in (SELECT * FROM moderators)
        )
;