SELECT count(*) FROM diffs
WHERE
    diffs.date_created < date(:Date, '+1 day')
    AND (diffs.date_closed is NULL OR diffs.date_closed >= date(:Date, '+1 day'))
    AND ((NOT :IsCommunity) OR
         diffs.author_phid NOT in (SELECT * FROM moderators)
        )
;