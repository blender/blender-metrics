WITH
/*
INPUT AS ( SELECT
    date("2020-05-31", "+1 day") AS tomorrow,
    1 AS needs_review,
    0 AS needs_revision,
    0 AS changes_planned,
    0 as accepted,
    0 AS draft,
    1 AS is_community
)
*/
INPUT AS ( SELECT
    date(:Date, "+1 day") AS tomorrow,
    :StatusNeedsReview AS needs_review,
    :StatusNeedsRevision AS needs_revision,
    :StatusChangesPlanned AS changes_planned,
    :StatusAccepted as accepted,
    :StatusDraft AS draft,
    :IsCommunity AS is_community
)
SELECT COUNT(*) FROM (

SELECT

(
    SELECT
        CASE
            WHEN last_status_event.status = 'accept'
            THEN 'accepted'
            WHEN last_status_event.status = 'reject'
            THEN 'needs-revision'
            WHEN last_status_event.status = 'reopen'
            THEN 'needs-review'
            WHEN last_status_event.status = 'request_review'
            THEN 'needs-review'
            WHEN last_status_event.status = 'rethink'
            THEN 'changes-planned'
        ELSE
            last_status_event.status
        END
    FROM
    (
		SELECT
			diffs.date_created AS date,
			"needs-review" as status
		UNION ALL
        SELECT
            t.date_created AS date,
            ts.new AS status
        FROM transactions_status ts
            LEFT JOIN transactions t ON ts.transaction_id = t.id
            WHERE t.diff_phid = diffs.phid
        UNION ALL
        SELECT
            t.date_created as date,
            "changes-planned" as status
        FROM transactions_planchanges tp
            LEFT JOIN transactions t ON tp.transaction_id = t.id
            WHERE t.diff_phid = diffs.phid
        UNION ALL
        SELECT
            t.date_created as date,
            "needs-review" as status
        FROM transactions_requestreview tr
            LEFT JOIN transactions t ON tr.transaction_id = t.id
            WHERE t.diff_phid = diffs.phid
        UNION ALL
        /* When a patch is updated, its status changes to needs review. */
        SELECT
            t.date_created as date,
            "needs-review" as status
        FROM transactions_update tu
            LEFT JOIN transactions t ON tu.transaction_id = t.id
            WHERE t.diff_phid = diffs.phid
        UNION ALL
        SELECT
            t.date_created as date,
            ta.new as status
        FROM transactions_action ta
            LEFT JOIN transactions t ON ta.transaction_id = t.id
            WHERE
                t.diff_phid = diffs.phid
            AND ta.new in (
                'abandon',
                'accept',
                --'claim',
                'commit',
                --'reclaim',
                'reject',
                'reopen',
                'request_review',
                --'resign',
                'rethink'
                )
        ORDER BY
            date DESC
        LIMIT 1
    ) AS last_status_event
) AS status_at_point_in_time

FROM
    diffs,
    INPUT
WHERE
    diffs.date_created < INPUT.tomorrow
    AND (diffs.date_closed is NULL OR diffs.date_closed >= INPUT.tomorrow)
    AND
        ((NOT INPUT.is_community) OR
            diffs.author_phid NOT in (SELECT * FROM moderators)
        )
    AND (
        (INPUT.needs_review AND status_at_point_in_time = 'needs-review') OR
        (INPUT.needs_revision AND status_at_point_in_time = 'needs-revision') OR
        (INPUT.changes_planned AND status_at_point_in_time = 'changes-planned') OR
        (INPUT.accepted AND status_at_point_in_time = 'accepted') OR
        (INPUT.draft AND status_at_point_in_time = 'draft')
    )
) -- COUNT
;