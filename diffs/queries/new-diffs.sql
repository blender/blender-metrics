SELECT count(*) FROM diffs
WHERE
    diffs.date_created >= :Date
    AND diffs.date_created < date(:Date, '+1 day')
    AND (
        (:AuthorIsModerator AND :AuthorIsCommunity) OR
        (:AuthorIsModerator AND
            (diffs.author_phid in (SELECT * FROM moderators))
         ) OR
        (:AuthorIsCommunity AND
            (diffs.author_phid not in (SELECT * FROM moderators))
        )
    )
;