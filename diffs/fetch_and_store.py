#!/usr/bin/env python3.8

"""Fetch and store Diffs and Transactions from developer.blender.org.

Needs to be run as module with `python -m diffs.fetch_and_store`.
This script allows to query the API (Conduit) of the Blender
development portal and fetch Differentials and their relative transactions.

The data is progressively fetched and saved into a local sqlite database,
for more efficient processing and data mining through other scripts.

This script requires the environment variable CONDUIT_API_TOKEN to
be set to a valid token.

The script works as follows:

    * main() is the entry point
    * fetch diffs from the server starting from `after`
        * fetch the transactions for each diff
        * save diff its transactions in the sqlite database
"""

import dataclasses as dc
import datetime
import logging
import os
from diffs.utils import blender_git_repository_get
import requests
import sqlite3
import sys
from contextlib import closing
from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    NewType,
    Optional,
    Tuple,
    TypeVar,
    cast,
)

import yarl
from more_itertools import chunked
from retry import retry as retry_decorator

from .utils import assert_cast, assert_cast_optional, setup_logging, data_dir, get_query

logger = logging.getLogger(__file__)

Cursor = NewType('Cursor', str)

F = TypeVar('F', bound=Callable[..., object])
retry: Callable[[F], F] = retry_decorator(
    tries=10, delay=1, backoff=2, logger=logger)


def init_sqlite(connection: sqlite3.Connection) -> None:
    connection.executescript(
        '''
        PRAGMA foreign_keys = ON;
        PRAGMA encoding = 'UTF-8';
        '''
    )
    connection.executescript(
        '''
        DROP table IF EXISTS moderators;

        CREATE TABLE moderators
        (
            phid              TEXT NOT NULL
        );

        CREATE UNIQUE INDEX IF NOT EXISTS moderators__phid
            ON moderators (phid);

        CREATE TABLE IF NOT EXISTS diffs
        (
            id                BIGINT NOT NULL,
            phid              TEXT NOT NULL,
            status            TEXT NOT NULL,
            author_phid       TEXT NOT NULL,
            date_created      DATETIME NOT NULL,
            date_modified     DATETIME NOT NULL,
            date_closed       DATETIME
        );

        CREATE UNIQUE INDEX IF NOT EXISTS diffs__id
            ON diffs (id);

        CREATE UNIQUE INDEX IF NOT EXISTS diffs__phid
            ON diffs (phid);

        CREATE INDEX IF NOT EXISTS diffs__date_created
            ON diffs (date_created);

        CREATE INDEX IF NOT EXISTS diffs__date_modified
            ON diffs (date_modified);

        CREATE TABLE IF NOT EXISTS transactions
        (
            id            BIGINT NOT NULL,
            phid          TEXT NOT NULL,
            diff_phid     TEXT NOT NULL REFERENCES diffs(phid),
            author_phid     TEXT NOT NULL,
            date_created  DATETIME NOT NULL,
            date_modified DATETIME
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions__id
            ON transactions (id);
        
        CREATE UNIQUE INDEX IF NOT EXISTS transactions__phid
            ON transactions (phid);
        
        CREATE INDEX IF NOT EXISTS transactions_diff_phid
            ON transactions (diff_phid);

        CREATE INDEX IF NOT EXISTS transactions__author_phid
            ON transactions (author_phid);

        CREATE INDEX IF NOT EXISTS transactions__first_update
            ON transactions(diff_phid, id, date_created);

        CREATE TABLE IF NOT EXISTS transactions_status
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            old            TEXT,
            new            TEXT NOT NULL
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_status__transaction_id
            ON transactions_status (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_abandon
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE TABLE IF NOT EXISTS transactions_action
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            old            TEXT,
            new            TEXT NOT NULL
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_action__transaction_id
            ON transactions_action (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_abandon
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_abandon__transaction_id
            ON transactions_abandon (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_close
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_close__transaction_id
            ON transactions_close (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_planchanges
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_planchanges__transaction_id
            ON transactions_planchanges (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_requestreview
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_requestreview__transaction_id
            ON transactions_requestreview (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_update
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_update__transaction_id
            ON transactions_update (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_null
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_null__transaction_id
            ON transactions_null (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_projects
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            operation      TEXT NOT NULL,
            phid           TEXT NOT NULL
        );

        CREATE INDEX IF NOT EXISTS transactions_projects__transaction_id
            ON transactions_projects (transaction_id);
        '''
    )


@dc.dataclass
class Diff:
    id: int
    phid: str
    author_phid: str
    status: str
    date_created: datetime.datetime
    date_modified: datetime.datetime
    date_closed: Optional[datetime.datetime]


@dc.dataclass
class Transaction:
    id: int
    phid: str
    diff_phid: str
    author_phid: str
    date_created: datetime.datetime
    date_modified: Optional[datetime.datetime]


@dc.dataclass
class AbandonTransaction(Transaction):
    pass


@dc.dataclass
class CloseTransaction(Transaction):
    """
    This includes automatically closed by phabricator or manually by a developer.
    """
    pass


@dc.dataclass
class PlanChangesTransaction(Transaction):
    pass


@dc.dataclass
class RequestReviewTransaction(Transaction):
    pass


@dc.dataclass
class NullTransaction(Transaction):
    """
    Old transactions, it can be either abandoned or closed.
    """
    pass


@dc.dataclass
class ActionTransaction(Transaction):
    old: Optional[str]
    new: str


@dc.dataclass
class StatusTransaction(Transaction):
    old: Optional[str]
    new: str


@dc.dataclass
class UpdateTransaction(Transaction):
    """Should be treated as need-review"""
    pass


@dc.dataclass
class ProjectsTransactionOperation:
    operation: str
    phid: str


@dc.dataclass
class ProjectsTransaction(Transaction):
    operations: List[ProjectsTransactionOperation]


def store_diff_and_transactions(
    diff: Diff, transactions: Iterable[Transaction], connection: sqlite3.Connection
) -> None:
    """Save a diff and its transactions, overriding existing entries."""
    cursor: sqlite3.Cursor
    with connection:
        connection.execute(
            '''
            INSERT INTO diffs (
                id,
                phid,
                author_phid,
                status,
                date_created,
                date_modified,
                date_closed
            )
            VALUES (
                :id,
                :phid,
                :author_phid,
                :status,
                :date_created,
                :date_modified,
                :date_closed
            )
            ON CONFLICT (id) DO UPDATE SET
                phid= :phid,
                author_phid= :author_phid,
                status= :status,
                date_created= :date_created,
                date_modified= :date_modified,
                date_closed= :date_closed;
            ''',
            dc.asdict(diff),
        )

        for chunk in chunked(transactions, n=256):
            connection.executemany(
                '''
                INSERT INTO transactions (
                    id,
                    phid,
                    diff_phid,
                    author_phid,
                    date_created,
                    date_modified
                )
                VALUES (
                    :id,
                    :phid,
                    :diff_phid,
                    :author_phid,
                    :date_created,
                    :date_modified
                )
                ON CONFLICT (id) DO UPDATE SET
                    phid= :phid,
                    diff_phid= :diff_phid,
                    author_phid= :author_phid,
                    date_created= :date_created,
                    date_modified= :date_modified;
                ''',
                [
                    {
                        'id': transaction.id,
                        'phid': transaction.phid,
                        'diff_phid': transaction.diff_phid,
                        'author_phid': transaction.author_phid,
                        'date_created': transaction.date_created,
                        'date_modified': transaction.date_modified,
                    }
                    for transaction in chunk
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_status (
                    transaction_id,
                    old,
                    new
                )
                VALUES (
                    :transaction_id,
                    :old,
                    :new
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    old= :old,
                    new= :new;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'old': transaction.old,
                        'new': transaction.new,
                    }
                    for transaction in chunk
                    if isinstance(transaction, StatusTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_action (
                    transaction_id,
                    old,
                    new
                )
                VALUES (
                    :transaction_id,
                    :old,
                    :new
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    old= :old,
                    new= :new;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'old': transaction.old,
                        'new': transaction.new,
                    }
                    for transaction in chunk
                    if isinstance(transaction, ActionTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_abandon (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, AbandonTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_close (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, CloseTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_planchanges (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, PlanChangesTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_requestreview (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, RequestReviewTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_update (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, UpdateTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_null (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, NullTransaction)
                ],
            )

            connection.executemany(
                '''
                DELETE FROM transactions_projects
                    WHERE transaction_id= :transaction_id;
                ''',
                [
                    {'transaction_id': transaction.id}
                    for transaction in chunk
                    if isinstance(transaction, ProjectsTransaction)
                ],
            )
            connection.executemany(
                '''
                INSERT INTO transactions_projects (
                    transaction_id,
                    operation,
                    phid
                )
                VALUES (
                    :transaction_id,
                    :operation,
                    :phid
                );
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'operation': operation.operation,
                        'phid': operation.phid,
                    }
                    for transaction in chunk
                    if isinstance(transaction, ProjectsTransaction)
                    for operation in transaction.operations
                ],
            )


def set_diff_close_date(
    diff: Diff, connection: sqlite3.Connection
) -> None:
    """Set the diff close date based on the abandon, close and nulls"""
    # Need a query that aggregates all the transactions for the closed tasks
    # and fallback to the last null transaction

    with closing(connection.cursor()) as cursor:
        query = get_query("set-closed-date.sql")
        cursor.execute(query, {
            "DiffId": diff.phid,
        })


def get_max_date_modified(connection: sqlite3.Connection) -> Optional[datetime.datetime]:
    """Get the date of the latest diff entry in the database.

    Returns:
        An optional datetime object.
    """
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        cursor.execute(
            '''
            SELECT coalesce(diffs.date_modified, diffs.date_created)
            FROM diffs
            ORDER BY coalesce(diffs.date_modified, diffs.date_created) DESC
            LIMIT 1;
            '''
        )
        result = cursor.fetchall()

    if not result:
        return None
    else:
        return assert_cast_optional(
            datetime.datetime, datetime.datetime.fromisoformat(result[0][0])
        )


@retry
def fetch_single(
    conduit_url: yarl.URL,
    api_token: str,
    method: str,
    data: Dict[str, str],
    cursor: Optional[Cursor] = None,
) -> Tuple[List[object], Optional[Cursor]]:
    """Generic function to query a single item from Conduit.

    Returns:
        A dictionary containing the item data.
    """
    final_data: Dict[str, str] = {**data, 'api.token': api_token}
    if cursor is not None:
        final_data['after'] = cursor

    logger.info(f"Calling {method} ({cursor=}, {data=}).")
    response = requests.post(str(conduit_url / method), data=final_data)
    response.raise_for_status()
    response_json = response.json()

    after = response_json['result']['cursor']['after']
    return response_json['result']['data'], None if after is None else Cursor(after)


def fetch_all(
    conduit_url: yarl.URL, api_token: str, method: str, data: Dict[str, str],
) -> Iterable[object]:
    """Generic function to query lists from Conduit.

    Yields:
        response_data - the result of fetch_single()
    """
    response_data, cursor = fetch_single(conduit_url, api_token, method, data)
    yield from response_data
    while cursor is not None:
        response_data, cursor = fetch_single(
            conduit_url, api_token, method, data, cursor=cursor)
        yield from response_data


def fields(obj: Dict[object, object]) -> Dict[object, object]:
    return assert_cast(dict, obj['fields'])


def fetch_diffs(
    conduit_url: yarl.URL,
    api_token: str,
    repository_phid: str,
    after: Optional[datetime.datetime] = None,
) -> Iterable[Diff]:
    """Query Conduit for all diffs that changed since "after".

    The query is not limited by any project.

    Yields:
        Diff objects.
    """
    modified_start: Dict[str, str]
    if after:
        modified_start = {
            'constraints[modifiedStart]': str(int(after.timestamp()))}
    else:
        modified_start = {}

    for diff in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            conduit_url,
            api_token,
            'differential.revision.search',
            data={
                'queryKey': 'all',
                # Blender queries don't all live in the Blender repository
                # e.g., D184
                # 'constraints[repositoryPHIDs][0]': str(repository_phid),
                'order[0]': '-updated',
                'order[1]': '-id',
                **modified_start,
            },
        ),
    ):
        yield Diff(
            id=assert_cast(int, diff.get('id')),
            phid=assert_cast(str, diff.get('phid')),
            author_phid=assert_cast(str, fields(diff).get('authorPHID')),
            status=assert_cast(str, assert_cast(
                dict, fields(diff).get('status', {})).get('value')),
            date_created=datetime.datetime.fromtimestamp(
                assert_cast(int, fields(diff)['dateCreated'])
            ),
            date_modified=datetime.datetime.fromtimestamp(
                assert_cast(int, fields(diff)['dateModified'])
            ),
            date_closed=(
                datetime.datetime.fromtimestamp(
                    assert_cast(int, fields(diff)['dateClosed']))
                if fields(diff).get('dateClosed') is not None
                else None
            ),
        )


def fetch_transactions(
    conduit_url: yarl.URL, api_token: str, diff_phid: str,
) -> Iterable[Transaction]:
    """Query Conduit for all the transactions that ever happened to a Diff."""
    for transaction in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            conduit_url, api_token, 'transaction.search', data={'objectIdentifier': str(diff_phid)},
        ),
    ):
        transaction_type = assert_cast_optional(str, transaction.get('type'))

        kwargs = Transaction(
            id=assert_cast(int, transaction.get('id')),
            phid=assert_cast(str, transaction.get('phid')),
            diff_phid=assert_cast(str, transaction.get('objectPHID')),
            author_phid=assert_cast(str, transaction.get('authorPHID')),
            date_created=datetime.datetime.fromtimestamp(
                assert_cast(int, transaction['dateCreated'])
            ),
            date_modified=(
                datetime.datetime.fromtimestamp(
                    assert_cast(int, transaction['dateModified']))
                if transaction.get('dateModified') is not None
                else None
            ),
        )

        if transaction_type == 'abandon':
            yield AbandonTransaction(
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'close':
            yield CloseTransaction(
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'differential:action':
            yield ActionTransaction(
                old=assert_cast_optional(str, fields(transaction).get('old')),
                new=assert_cast(str, fields(transaction).get('new')),
                **dc.asdict(kwargs),
            )
        # The old transactions have type NULL even though they may
        # close (transaction: 44, patch D22); or
        # abandon (transaction: 112, patch D9)
        elif transaction_type == None:
            yield NullTransaction(
                **dc.asdict(kwargs),
            )
        # Note, the status in the diff is "changed-planned", but the
        # type of the transaction itself is "plan-changes"
        elif transaction_type == 'plan-changes':
            yield PlanChangesTransaction(
                **dc.asdict(kwargs),
            )
        # Note, the status in the diff is "needs-review", but the
        # type of the transaction itself is "request-review"
        elif transaction_type == 'request-review':
            yield RequestReviewTransaction(
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'update':
            yield UpdateTransaction(
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'status':
            yield StatusTransaction(
                old=assert_cast_optional(str, fields(transaction).get('old')),
                new=assert_cast(str, fields(transaction).get('new')),
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'projects':
            yield ProjectsTransaction(
                operations=[
                    ProjectsTransactionOperation(
                        operation=assert_cast(str, assert_cast(
                            dict, operation).get('operation')),
                        phid=assert_cast(str, assert_cast(
                            dict, operation).get('phid')),
                    )
                    for operation in assert_cast(list, fields(transaction).get('operations'))
                ],
                **dc.asdict(kwargs),
            )


@dc.dataclass
class Moderator:
    phid: str


def fetch_moderators(
    conduit_url: yarl.URL,
    api_token: str,
    project_phid: str,
) -> List[Moderator]:
    """Query Conduit for all the members of a project

    This is used to filter out diffs created by "the community"
    and by the development team(volunteers and paid developers).

    Yields:
        Moderator objects.
    """

    response_data, cursor = result = fetch_single(
        conduit_url,
        api_token,
        'project.query',
        data={
            'phids[0]': str(project_phid)
        },
    )

    moderators = list()
    for moderator_phid in cast(
        Iterable[List[str]],
        response_data[project_phid]['members'],
    ):
        moderators.append(Moderator(phid=moderator_phid))

    return moderators


def store_moderators(
    moderators: Iterable[Moderator], connection: sqlite3.Connection
) -> None:
    """Save the latest moderators list, deleting all existing entries."""
    cursor: sqlite3.Cursor

    logger.info(f'Storing {len(moderators)} moderators.')

    with connection:
        for chunk in chunked(moderators, n=256):
            connection.executemany(
                '''
                INSERT INTO moderators (
                    phid
                )
                VALUES (
                    :phid
                )
                ''',
                [
                    {
                        'phid': moderator.phid,
                    }
                    for moderator in chunk
                ],
            )


def main() -> None:
    api_token = os.environ['CONDUIT_API_TOKEN']

    data_dir.mkdir(exist_ok=True)

    setup_logging(logger, 'log-diff.txt')

    conduit_url = yarl.URL('https://developer.blender.org/api/')
    blender_repository_phid = 'PHID-REPO-jecxaju7eq2vibswb3st'
    moderators_project_phid = 'PHID-PROJ-2mzqsc6avm7hfpxuv7ub'

    with closing(sqlite3.connect(str(data_dir / 'diffs.sqlite'))) as connection:
        init_sqlite(connection)

        after: Optional[datetime.datetime]
        if len(sys.argv) > 1 and sys.argv[1]:
            after = datetime.datetime.fromisoformat(sys.argv[1])
        else:
            after = get_max_date_modified(connection)

        if after is not None:
            logger.info(f'Fetching updates after: {after.isoformat(sep=" ")}')

        for i, diff in enumerate(
            fetch_diffs(conduit_url, api_token,
                        blender_repository_phid, after=after)
        ):
            transactions = fetch_transactions(
                conduit_url, api_token, diff_phid=diff.phid)
            store_diff_and_transactions(diff, transactions, connection)
            set_diff_close_date(diff, connection)

            if i % 10 == 0:
                current_after = diff.date_modified

                if current_after is None:
                    logger.info(f'Fetched {i + 1} diffs.')
                else:
                    logger.info(
                        f'Fetched {i + 1} diffs. We now have all modifications after: {current_after.isoformat(sep=" ")}'
                    )

        # Get a new moderators list every time
        logger.info(f'Fetching moderators list')
        moderators = fetch_moderators(
            conduit_url, api_token, moderators_project_phid)
        store_moderators(moderators, connection)


if __name__ == '__main__':
    main()
