#!/usr/bin/env python3.8

from argparse import ArgumentParser
import os
import dataclasses as dc
import datetime
import logging
import sqlite3
from contextlib import closing
from .utils import assert_cast, setup_logging, data_dir, get_query
from . import plot_days
from .plot_days import Query
from enum import Enum

logger = logging.getLogger(__file__)


@dc.dataclass
class InputArguments:
    date_start: datetime.datetime
    date_end: datetime.datetime
    force_reset: bool


def process_arguments() -> InputArguments:
    """Mode is mandatory, date range is optional."""
    parser = ArgumentParser(
        description='''
    Count tracker items for a range of days.
    By default will get today's data, for tasks that need developers attention.
    '''
    )
    parser.add_argument(
        '--start',
        type=datetime.datetime.fromisoformat,
        help='''
        first day of range, yesterday as default
        ''',
    )
    parser.add_argument(
        '--end',
        type=datetime.datetime.fromisoformat,
        help='''
        last day of range, optional, yesterday as default
        ''',
    )
    parser.add_argument(
        '--force-reset',
        dest='force_reset',
        action='store_true',
        help='''
        reset the plot database
        ''',
    )
    parser.set_defaults(force_reset=False)

    arguments_raw = parser.parse_args()

    if arguments_raw.start is None:
        if arguments_raw.end is not None:
            parser.error(
                "The --end argument can only be used when --start is specified")
        else:
            arguments_raw.start = arguments_raw.end = datetime.datetime.today() - \
                datetime.timedelta(days=1)

    elif arguments_raw.end is None:
        arguments_raw.end = datetime.datetime.today() - datetime.timedelta(days=1)

    elif arguments_raw.end < arguments_raw.start:
        parser.error("The --end argument needs to be later than --start")

    input_arguments = InputArguments(
        arguments_raw.start, arguments_raw.end, arguments_raw.force_reset)
    return input_arguments


def get_query_id(connection: sqlite3.Connection, query_name: Query) -> int:
    """Return the query id for a query name"""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("plotted-queries-id.sql")
        cursor.execute(query, {
            "QueryName": query_name,
        })

        result = cursor.fetchall()
        query_id = assert_cast(int, result[0][0])

        if query_id is not None:
            return query_id

    # No query found
    raise Exception


def create_tables_scheme(connection: sqlite3.Connection):
    """Create the tables scheme for the database."""

    connection.executescript('''
    DROP TABLE IF EXISTS "queries";
    DROP TABLE IF EXISTS "values";
    ''')

    connection.executescript('''
    CREATE TABLE "queries" (
        "id"    INTEGER,
        "name"  TEXT,
        "ui_name"       INTEGER,
        "description"   INTEGER,
        PRIMARY KEY("id")
    );

    CREATE TABLE "values" (
            "id"    INTEGER,
            "date"  TEXT,
            "count" INTEGER,
            "query" INTEGER,
            PRIMARY KEY("id"),
            FOREIGN KEY("query") REFERENCES "queries"("id")
    )
    ''')


def populate_queries_table(connection: sqlite3.Connection):
    """
    Populate the queries table.
    """
    cursor: sqlite3.Cursor

    query_lookup = {
        Query.OPEN_DIFFS: ("Open Patches", "All the open patches"),
        Query.OPEN_DIFFS_COMMUNITY: ("Open Community Patches", "All the open community patches"),
        Query.NEW_DIFFS_ALL: ("New Patches", "All the new patches"),
        Query.NEW_DIFFS_MODERATORS: ("New Moderators Patches", "New patches where author is in Moderators group"),
        Query.NEW_DIFFS_COMMUNITY: ("New Community Patches", "New patches where author is not in Moderators group"),
        Query.CLOSED_DIFFS_ALL: ("Closed Patches", "All the closed patches"),
        Query.CLOSED_DIFFS_ALL_COMMUNITY: ("Closed Community Patches", "All the closed community patches"),
        Query.CLOSED_DIFFS_PUBLISHED: ("Published Patches", "All the closed patches with status of published, automatically or manually"),
        Query.CLOSED_DIFFS_PUBLISHED_COMMUNITY: ("Published Community Patches", "All the closed community patches with status of published, automatically or manually"),
        Query.CLOSED_DIFFS_ABANDONED: ("Abandoned Patches", "All the closed patches with status of abandoned"),
        Query.CLOSED_DIFFS_ABANDONED_COMMUNITY: ("Abandoned Community Patches", "All the closed community patches with status of abandoned"),
        Query.OPEN_DIFFS_ACCEPTED: ("Accepted Patches", "All the open patches with status of accepted"),
        Query.OPEN_DIFFS_CHANGES_PLANNED: ("Changes Planned Patches", "All the open patches with status of changes planned"),
        Query.OPEN_DIFFS_DRAFT: ("Draft Patches", "All the open patches with status of draft"),
        Query.OPEN_DIFFS_NEEDS_REVIEW: ("Needs Review Patches", "All the open patches with status of needs review"),
        Query.OPEN_DIFFS_NEEDS_REVISION: ("Needs Revision Patches", "All the open patches with status of needs revision"),
        Query.OPEN_DIFFS_ACCEPTED_COMMUNITY: ("Accepted Community Patches", "All the open community patches with status of accepted"),
        Query.OPEN_DIFFS_CHANGES_PLANNED_COMMUNITY: ("Changes Planned Community Patches", "All the open community patches with status of changes planned"),
        Query.OPEN_DIFFS_DRAFT_COMMUNITY: ("Draft Community Patches", "All the open community patches with status of draft"),
        Query.OPEN_DIFFS_NEEDS_REVIEW_COMMUNITY: ("Needs Review Community Patches", "All the open community patches with status of needs review"),
        Query.OPEN_DIFFS_NEEDS_REVISION_COMMUNITY: ("Needs Revision Community Patches", "All the open community patches with status of needs revision"),
    }

    with closing(connection.cursor()) as cursor:
        for query in Query:
            if query.value == str(Query.DEBUG_RESET):
                continue
            query_name = query.value

            full_name, description = query_lookup.get(query, ("", ""))
            sql_query = '''
            WITH INPUT AS (SELECT
                :QueryName AS name,
                :FullName AS ui_name,
                :Description AS description)

            INSERT INTO "queries" (name, ui_name, description)
            SELECT name, ui_name, description FROM INPUT;
            '''
            cursor.execute(sql_query, {
                "QueryName": query_name,
                "FullName": full_name,
                "Description": description,
            })


def insert_plotted_value(connection: sqlite3.Connection, date: datetime.datetime, count: int, query_id: int) -> bool:
    """Update the database with the new values"""
    cursor: sqlite3.Cursor
    logger.debug("Adding new value: date={date}, count={count}, query_id={query_id}".format(
        date=date.strftime("%Y-%m-%d"), count=count, query_id=query_id))

    with closing(connection.cursor()) as cursor:
        query = get_query("insert-plot-values.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Count": count,
            "QueryId": query_id,
        })

    return False


def main() -> None:
    setup_logging(logger, 'log-diff-update-plotted-data.txt')
    arguments = process_arguments()
    output_database_url = str(data_dir / 'diffs-plot.sqlite')

    if arguments.force_reset or not os.path.exists(output_database_url):
        logger.info("Creating database: " + output_database_url)
        # We need run this before the main loop to be sure the database
        # was written to before we add new data.
        with closing(sqlite3.connect(output_database_url)) as output_connection:
            create_tables_scheme(output_connection)
            populate_queries_table(output_connection)

    with closing(sqlite3.connect(output_database_url)) as output_connection:
        with closing(sqlite3.connect(str(data_dir / 'diffs.sqlite'))) as input_connection:

            day = arguments.date_start
            last_day = arguments.date_end
            delta = datetime.timedelta(days=1)

            while day <= last_day:
                # query iterator should be refactor away
                all_queries = []
                for query in Query:
                    if query.value == str(Query.DEBUG_RESET):
                        continue
                    query_id = get_query_id(output_connection, query.value)

                    today = plot_days.Day(day, query)
                    logger.debug("Processing date: {date}".format(
                        date=day.strftime("%Y-%m-%d")))

                    # All the computation happens here
                    today.process(input_connection)

                    # Update database
                    insert_plotted_value(
                        output_connection, today.date, today.total, query_id)

                # Tomorrow
                day += delta


if __name__ == "__main__":
    main()
