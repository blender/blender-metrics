import dataclasses as dc
import datetime
import sqlite3
from itertools import chain
from typing import Dict, List, Any, Tuple, Type, TypeVar

import pymongo.database
from more_itertools import chunked
from pymongo import MongoClient

A = TypeVar('A')


def type_assert(tp: Type[A], x: object) -> A:
    assert isinstance(x, tp)
    return x


@dc.dataclass
class User:
    id: str
    created: datetime.datetime


@dc.dataclass
class UserRole:
    user_id: str
    role: str


@dc.dataclass
class UserGroup:
    user_id: str
    group_id: str


@dc.dataclass
class UserViewProgress:
    user_id: str
    video_id: str
    done: bool
    last_watched: datetime.datetime
    progress_in_percent: int
    progress_in_sec: float


def clean_and_split_user(
    user: Dict[str, Any]
) -> Tuple[User, List[UserRole], List[UserGroup], List[UserViewProgress]]:
    user_id = str(user['_id'])
    return (
        User(id=user_id, created=type_assert(datetime.datetime, user['_created'])),
        [UserRole(user_id=user_id, role=type_assert(str, role)) for role in user.get('roles', [])],
        [UserGroup(user_id=user_id, group_id=str(group_id)) for group_id in user['groups']],
        [
            UserViewProgress(
                user_id=user_id,
                video_id=str(video_id),
                done=type_assert(bool, props.get('done', False)),
                last_watched=type_assert(datetime.datetime, props['last_watched']),
                progress_in_percent=type_assert(int, props['progress_in_percent']),
                progress_in_sec=type_assert(float, props['progress_in_sec']),
            )
            for video_id, props in user.get('nodes', {}).get('view_progress', {}).items()
        ],
    )


def load_users(mongo_db: pymongo.database.Database, sql_connection: sqlite3.Connection) -> None:
    sql_connection.executescript(
        '''
        DROP TABLE IF EXISTS users;
        CREATE TABLE users (
            id TEXT NOT NULL PRIMARY KEY,
            created TEXT NOT NULL
        );
        
        DROP TABLE IF EXISTS users_roles;
        CREATE TABLE users_roles (
            user_id TEXT NOT NULL REFERENCES users(id),
            role TEXT NOT NULL
        );
        
        DROP TABLE IF EXISTS users_groups;
        CREATE TABLE users_groups (
            user_id TEXT NOT NULL REFERENCES users(id),
            group_id TEXT NOT NULL
        );
        
        DROP TABLE IF EXISTS users_view_progress;
        CREATE TABLE users_view_progress (
            user_id TEXT NOT NULL REFERENCES users(id),
            video_id TEXT NOT NULL,
            done BOOLEAN NOT NULL,
            last_watched TEXT NOT NULL,
            progress_in_percent INTEGER NOT NULL,
            progress_in_sec REAL NOT NULL
        )
        '''
    )

    for chunk in chunked(mongo_db['users'].find(), n=256):
        users, roles, groups, view_progresses = zip(*map(clean_and_split_user, chunk))
        sql_connection.executemany(
            '''
            INSERT INTO users (id, created) VALUES (:id, :created);
            ''',
            map(dc.asdict, users),
        )
        sql_connection.executemany(
            '''
            INSERT INTO users_roles (user_id, role) VALUES (:user_id, :role);
            ''',
            map(dc.asdict, chain(*roles)),
        )
        sql_connection.executemany(
            '''
            INSERT INTO users_groups (user_id, group_id) VALUES (:user_id, :group_id);
            ''',
            map(dc.asdict, chain(*groups)),
        )
        sql_connection.executemany(
            '''
            INSERT INTO users_view_progress (
                user_id,
                video_id,
                done,
                last_watched,
                progress_in_percent,
                progress_in_sec
            ) VALUES (
                :user_id,
                :video_id,
                :done,
                :last_watched,
                :progress_in_percent,
                :progress_in_sec
            );
            ''',
            map(dc.asdict, chain(*view_progresses)),
        )


@dc.dataclass
class Project:
    id: str
    user_id: str
    name: str
    created: datetime.datetime
    updated: datetime.datetime
    deleted: bool
    category: str
    is_private: bool


@dc.dataclass
class ProjectGroup:
    project_id: str
    group_id: str


def clean_and_split_project(project: Dict[str, Any]) -> Tuple[Project, List[ProjectGroup]]:
    project_id = str(project['_id'])
    return (
        Project(
            id=project_id,
            user_id=str(project['user']),
            name=type_assert(str, project['name']),
            created=type_assert(datetime.datetime, project['_created']),
            updated=type_assert(datetime.datetime, project['_updated']),
            deleted=type_assert(bool, project['_deleted']),
            category=type_assert(str, project['category']),
            is_private=type_assert(bool, project['is_private']),
        ),
        [
            ProjectGroup(project_id=project_id, group_id=str(group_permissions['group']))
            for group_permissions in project.get('permissions', {}).get('groups', [])
        ],
    )


def load_projects(mongo_db: pymongo.database.Database, sql_connection: sqlite3.Connection) -> None:
    sql_connection.executescript(
        '''
        DROP TABLE IF EXISTS projects;
        CREATE TABLE projects (
            id TEXT NOT NULL PRIMARY KEY,
            user_id TEXT NOT NULL REFERENCES users(id),
            name TEXT NOT NULL,
            created TEXT NOT NULL,
            updated TEXT NOT NULL,
            deleted BOOLEAN NOT NULL,
            category TEXT NOT NULL,
            is_private BOOLEAN NOT NULL
        );

        DROP TABLE IF EXISTS projects_groups;
        CREATE TABLE projects_groups (
            project_id TEXT NOT NULL REFERENCES projects(id),
            group_id TEXT NOT NULL
        );
        '''
    )

    for chunk in chunked(mongo_db['projects'].find(), n=256):
        projects, groups = zip(*map(clean_and_split_project, chunk))
        sql_connection.executemany(
            '''
            INSERT INTO projects (
                id,
                user_id,
                name,
                created,
                updated,
                deleted,
                category,
                is_private
            ) VALUES (
                :id,
                :user_id,
                :name,
                :created,
                :updated,
                :deleted,
                :category,
                :is_private
            );
            ''',
            map(dc.asdict, projects),
        )
        sql_connection.executemany(
            '''
            INSERT INTO projects_groups (project_id, group_id) VALUES (:project_id, :group_id);
            ''',
            map(dc.asdict, chain(*groups)),
        )


@dc.dataclass
class Node:
    id: str
    project_id: str
    user_id: str
    created: datetime.datetime
    updated: datetime.datetime
    deleted: bool
    node_type: str
    name: str


def clean_node(node: Dict[str, Any]) -> Node:
    return Node(
        id=str(node['_id']),
        project_id=str(node['project']),
        user_id=str(node['user']),
        created=type_assert(datetime.datetime, node['_created']),
        updated=type_assert(datetime.datetime, node['_updated']),
        deleted=type_assert(bool, node.get('_deleted', False)),
        node_type=type_assert(str, node['node_type']),
        name=type_assert(str, node['name']),
    )


def load_nodes(mongo_db: pymongo.database.Database, sql_connection: sqlite3.Connection) -> None:
    sql_connection.executescript(
        '''
        DROP TABLE IF EXISTS nodes;
        CREATE TABLE nodes (
            id TEXT NOT NULL PRIMARY KEY,
            project_id TEXT NOT NULL REFERENCES projects(id),
            user_id TEXT NOT NULL REFERENCES users(id),
            created TEXT NOT NULL,
            updated TEXT NOT NULL,
            deleted BOOLEAN NOT NULL,
            node_type TEXT NOT NULL,
            name TEXT NOT NULL
        );
        '''
    )

    for chunk in chunked(mongo_db['nodes'].find(), n=256):
        nodes = map(clean_node, chunk)
        sql_connection.executemany(
            '''
            INSERT INTO nodes (
                id,
                project_id,
                user_id,
                created,
                updated,
                deleted,
                node_type,
                name
            ) VALUES (
                :id,
                :project_id,
                :user_id,
                :created,
                :updated,
                :deleted,
                :node_type,
                :name
            );
            ''',
            map(dc.asdict, nodes),
        )


def main() -> None:
    with MongoClient() as mongo_client, sqlite3.connect('data/data.sqlite') as sql_connection:
        mongo_db = mongo_client['cloud']

        load_users(mongo_db, sql_connection)
        load_projects(mongo_db, sql_connection)
        load_nodes(mongo_db, sql_connection)


if __name__ == '__main__':
    main()
