#!/usr/bin/env python3.8

"""Fetch and store Tasks and Transactions from developer.blender.org.

Needs to be run as module with `python -m tasks.fetch_and_store`.
This script allows to query the API (Conduit) of the Blender
development portal and fetch Tasks and their relative transactions
for the BF-Blender project. The data is progressively fetched and
saved into a local sqlite database, for more efficient processing
and data mining through other scripts.

This script requires the environment variable CONDUIT_API_TOKEN to
be set to a valid token.

The script works as follows:

    * main() is the entry point
    * fetch tasks from the server starting from `after`
        * fetch the transactions for each task
        * save task its transactions in the sqlite database
"""

import dataclasses as dc
import datetime
import logging
import os
import requests
import sqlite3
import sys
from contextlib import closing
from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    NewType,
    Optional,
    Tuple,
    TypeVar,
    cast,
)

import yarl
from more_itertools import chunked
from retry import retry as retry_decorator

from .utils import assert_cast, assert_cast_optional, setup_logging, data_dir

logger = logging.getLogger(__file__)

Cursor = NewType('Cursor', str)

F = TypeVar('F', bound=Callable[..., object])
retry: Callable[[F], F] = retry_decorator(
    tries=10, delay=1, backoff=2, logger=logger)


def init_sqlite(connection: sqlite3.Connection) -> None:
    connection.executescript(
        '''
        PRAGMA foreign_keys = ON;
        PRAGMA encoding = 'UTF-8';
        '''
    )
    connection.executescript(
        '''
        DROP table IF EXISTS moderators;

        CREATE TABLE moderators
        (
            phid              TEXT NOT NULL
        );

        CREATE UNIQUE INDEX IF NOT EXISTS moderators__phid
            ON moderators (phid);

        CREATE TABLE IF NOT EXISTS tasks
        (
            id                BIGINT NOT NULL,
            phid              TEXT NOT NULL,
            priority          INT NOT NULL,
            status            TEXT NOT NULL,
            subtype           TEXT NOT NULL,
            author_phid       TEXT NOT NULL,
            owner_phid        TEXT,
            bf_blender        INT NOT NULL DEFAULT 1,
            date_created      DATETIME NOT NULL,
            date_modified     DATETIME NOT NULL,
            date_closed       DATETIME
        );
        
        CREATE UNIQUE INDEX IF NOT EXISTS tasks__id
            ON tasks (id);
        
        CREATE UNIQUE INDEX IF NOT EXISTS tasks__phid
            ON tasks (phid);
        
        CREATE INDEX IF NOT EXISTS tasks__date_created
            ON tasks (date_created);
        
        CREATE INDEX IF NOT EXISTS tasks__date_modified
            ON tasks (date_modified);
        
        CREATE TABLE IF NOT EXISTS transactions
        (
            id            BIGINT NOT NULL,
            phid          TEXT NOT NULL,
            task_phid     TEXT NOT NULL REFERENCES tasks(phid),
            author_phid     TEXT NOT NULL,
            date_created  DATETIME NOT NULL,
            date_modified DATETIME
        );
        
        CREATE UNIQUE INDEX IF NOT EXISTS transactions__id
            ON transactions (id);
        
        CREATE UNIQUE INDEX IF NOT EXISTS transactions__phid
            ON transactions (phid);
        
        CREATE INDEX IF NOT EXISTS transactions__task_phid
            ON transactions (task_phid);

        CREATE INDEX IF NOT EXISTS transactions__author_phid
            ON transactions (author_phid);

        CREATE INDEX IF NOT EXISTS transactions__first_update
            ON transactions(task_phid, id, date_created);

        CREATE TABLE IF NOT EXISTS transactions_priority
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            old            INT,
            new            INT NOT NULL
        );
        
        CREATE UNIQUE INDEX IF NOT EXISTS transactions_priority__transaction_id
            ON transactions_priority (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_status
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            old            TEXT,
            new            TEXT NOT NULL
        );
        
        CREATE UNIQUE INDEX IF NOT EXISTS transactions_status__transaction_id
            ON transactions_status (transaction_id);
        
        CREATE TABLE IF NOT EXISTS transactions_subtype
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            old            TEXT,
            new            TEXT NOT NULL
        );
        
        CREATE UNIQUE INDEX IF NOT EXISTS transactions_subtype__transaction_id
            ON transactions_subtype (transaction_id);
        
        CREATE TABLE IF NOT EXISTS transactions_mergedinto
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
             -- We do not use a FK here because fetching the tasks in an order
             -- that preserves referential integrity becomes annoying quickly.
            into_task_phid BIGINT NOT NULL
        );
        
        CREATE UNIQUE INDEX IF NOT EXISTS transactions_mergedinto__transaction_id
            ON transactions_mergedinto (transaction_id);
        
        CREATE INDEX IF NOT EXISTS transactions_mergedinto__into_task_phid
            ON transactions_mergedinto (into_task_phid);
        
        CREATE TABLE IF NOT EXISTS transactions_owner
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            old            TEXT,
            new            TEXT
        );
        
        CREATE UNIQUE INDEX IF NOT EXISTS transactions_owner__transaction_id
            ON transactions_owner (transaction_id);
        
        CREATE TABLE IF NOT EXISTS transactions_projects
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            operation      TEXT NOT NULL,
            phid           TEXT NOT NULL
        );
        
        CREATE INDEX IF NOT EXISTS transactions_projects__transaction_id
            ON transactions_projects (transaction_id);
        '''
    )


@dc.dataclass
class Task:
    id: int
    phid: str
    priority: int
    status: str
    subtype: str
    author_phid: str
    owner_phid: Optional[str]
    date_created: datetime.datetime
    date_modified: datetime.datetime
    date_closed: Optional[datetime.datetime]


@dc.dataclass
class Transaction:
    id: int
    phid: str
    task_phid: str
    author_phid: str
    date_created: datetime.datetime
    date_modified: Optional[datetime.datetime]


@dc.dataclass
class PriorityTransaction(Transaction):
    old: Optional[int]
    new: int


@dc.dataclass
class SubtypeTransaction(Transaction):
    old: Optional[str]
    new: str


@dc.dataclass
class StatusTransaction(Transaction):
    old: Optional[str]
    new: str


@dc.dataclass
class MergedIntoTransaction(Transaction):
    into_task_phid: str


@dc.dataclass
class OwnerTransaction(Transaction):
    old: Optional[str]
    new: Optional[str]


@dc.dataclass
class ProjectsTransactionOperation:
    operation: str
    phid: str


@dc.dataclass
class ProjectsTransaction(Transaction):
    operations: List[ProjectsTransactionOperation]


def store_task_and_transactions(
    task: Task, transactions: Iterable[Transaction], connection: sqlite3.Connection
) -> None:
    """Save a task and its transactions, overriding existing entries."""
    cursor: sqlite3.Cursor
    with connection:
        connection.execute(
            '''
            INSERT INTO tasks (
                id,
                phid,
                priority,
                status,
                subtype,
                author_phid,
                owner_phid,
                date_created,
                date_modified,
                date_closed
            )
            VALUES (
                :id,
                :phid,
                :priority,
                :status,
                :subtype,
                :author_phid,
                :owner_phid,
                :date_created,
                :date_modified,
                :date_closed
            )
            ON CONFLICT (id) DO UPDATE SET
                phid= :phid,
                priority= :priority,
                status= :status,
                subtype= :subtype,
                author_phid= :author_phid,
                owner_phid= :owner_phid,
                date_created= :date_created,
                date_modified= :date_modified,
                date_closed= :date_closed;
            ''',
            dc.asdict(task),
        )

        for chunk in chunked(transactions, n=256):
            connection.executemany(
                '''
                INSERT INTO transactions (
                    id,
                    phid,
                    task_phid,
                    author_phid,
                    date_created,
                    date_modified
                )
                VALUES (
                    :id,
                    :phid,
                    :task_phid,
                    :author_phid,
                    :date_created,
                    :date_modified
                )
                ON CONFLICT (id) DO UPDATE SET
                    phid= :phid,
                    task_phid= :task_phid,
                    author_phid= :author_phid,
                    date_created= :date_created,
                    date_modified= :date_modified;
                ''',
                [
                    {
                        'id': transaction.id,
                        'phid': transaction.phid,
                        'task_phid': transaction.task_phid,
                        'author_phid': transaction.author_phid,
                        'date_created': transaction.date_created,
                        'date_modified': transaction.date_modified,
                    }
                    for transaction in chunk
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_priority (
                    transaction_id,
                    old,
                    new
                )
                VALUES (
                    :transaction_id,
                    :old,
                    :new
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    old= :old,
                    new= :new;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'old': transaction.old,
                        'new': transaction.new,
                    }
                    for transaction in chunk
                    if isinstance(transaction, PriorityTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_status (
                    transaction_id,
                    old,
                    new
                )
                VALUES (
                    :transaction_id,
                    :old,
                    :new
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    old= :old,
                    new= :new;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'old': transaction.old,
                        'new': transaction.new,
                    }
                    for transaction in chunk
                    if isinstance(transaction, StatusTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_subtype (
                    transaction_id,
                    old,
                    new
                )
                VALUES (
                    :transaction_id,
                    :old,
                    :new
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    old= :old,
                    new= :new;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'old': transaction.old,
                        'new': transaction.new,
                    }
                    for transaction in chunk
                    if isinstance(transaction, SubtypeTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_mergedinto (
                    transaction_id,
                    into_task_phid
                )
                VALUES (
                    :transaction_id,
                    :into_task_phid
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    into_task_phid= :into_task_phid;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'into_task_phid': transaction.into_task_phid,
                    }
                    for transaction in chunk
                    if isinstance(transaction, MergedIntoTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_owner (
                    transaction_id,
                    old,
                    new
                )
                VALUES (
                    :transaction_id,
                    :old,
                    :new
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    old= :old,
                    new= :new;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'old': transaction.old,
                        'new': transaction.new,
                    }
                    for transaction in chunk
                    if isinstance(transaction, OwnerTransaction)
                ],
            )

            connection.executemany(
                '''
                DELETE FROM transactions_projects
                    WHERE transaction_id= :transaction_id;
                ''',
                [
                    {'transaction_id': transaction.id}
                    for transaction in chunk
                    if isinstance(transaction, ProjectsTransaction)
                ],
            )
            connection.executemany(
                '''
                INSERT INTO transactions_projects (
                    transaction_id,
                    operation,
                    phid
                )
                VALUES (
                    :transaction_id,
                    :operation,
                    :phid
                );
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'operation': operation.operation,
                        'phid': operation.phid,
                    }
                    for transaction in chunk
                    if isinstance(transaction, ProjectsTransaction)
                    for operation in transaction.operations
                ],
            )


def get_max_date_modified(connection: sqlite3.Connection) -> Optional[datetime.datetime]:
    """Get the date of the latest task entry in the database.

    Returns:
        An optional datetime object.
    """
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        cursor.execute(
            '''
            SELECT coalesce(tasks.date_modified, tasks.date_created)
            FROM tasks
            ORDER BY coalesce(tasks.date_modified, tasks.date_created) DESC
            LIMIT 1;
            '''
        )
        result = cursor.fetchall()

    if not result:
        return None
    else:
        return assert_cast_optional(
            datetime.datetime, datetime.datetime.fromisoformat(result[0][0])
        )


@retry
def fetch_single(
    conduit_url: yarl.URL,
    api_token: str,
    method: str,
    data: Dict[str, str],
    cursor: Optional[Cursor] = None,
) -> Tuple[List[object], Optional[Cursor]]:
    """Generic function to query a single item from Conduit.

    Returns:
        A dictionary containing the item data.
    """
    final_data: Dict[str, str] = {**data, 'api.token': api_token}
    if cursor is not None:
        final_data['after'] = cursor

    logger.info(f"Calling {method} ({cursor=}, {data=}).")
    response = requests.post(str(conduit_url / method), data=final_data)
    response.raise_for_status()
    response_json = response.json()

    after = response_json['result']['cursor']['after']
    return response_json['result']['data'], None if after is None else Cursor(after)


def fetch_all(
    conduit_url: yarl.URL, api_token: str, method: str, data: Dict[str, str],
) -> Iterable[object]:
    """Generic function to query lists from Conduit.

    Yields:
        response_data - the result of fetch_single()
    """
    response_data, cursor = fetch_single(conduit_url, api_token, method, data)
    yield from response_data
    while cursor is not None:
        response_data, cursor = fetch_single(
            conduit_url, api_token, method, data, cursor=cursor)
        yield from response_data


def fields(obj: Dict[object, object]) -> Dict[object, object]:
    return assert_cast(dict, obj['fields'])


def fetch_tasks(
    conduit_url: yarl.URL,
    api_token: str,
    project_phid: str,
    after: Optional[datetime.datetime] = None,
) -> Iterable[Task]:
    """Query Conduit for all tasks that changed since "after".

    The query is limited by "project_phid" (often BF_BLENDER), and includes tasks that are
    tagged in milestones from a project (e.g., Blender 2.83).

    Yields:
        Task objects.
    """
    modified_start: Dict[str, str]
    if after:
        modified_start = {
            'constraints[modifiedStart]': str(int(after.timestamp()))}
    else:
        modified_start = {}

    for task in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            conduit_url,
            api_token,
            'maniphest.search',
            data={
                'constraints[projects][0]': str(project_phid),
                'order[0]': '-updated',
                'order[1]': '-id',
                **modified_start,
            },
        ),
    ):
        yield Task(
            id=assert_cast(int, task.get('id')),
            phid=assert_cast(str, task.get('phid')),
            priority=assert_cast(int, assert_cast(
                dict, fields(task).get('priority', {})).get('value')),
            status=assert_cast(str, assert_cast(
                dict, fields(task).get('status', {})).get('value')),
            subtype=assert_cast(str, fields(task).get('subtype')),
            author_phid=assert_cast(str, fields(task).get('authorPHID')),
            owner_phid=assert_cast_optional(
                str, fields(task).get('ownerPHID')),
            date_created=datetime.datetime.fromtimestamp(
                assert_cast(int, fields(task)['dateCreated'])
            ),
            date_modified=datetime.datetime.fromtimestamp(
                assert_cast(int, fields(task)['dateModified'])
            ),
            date_closed=(
                datetime.datetime.fromtimestamp(
                    assert_cast(int, fields(task)['dateClosed']))
                if fields(task).get('dateClosed') is not None
                else None
            ),
        )


def fetch_transactions(
    conduit_url: yarl.URL, api_token: str, task_phid: str,
) -> Iterable[Transaction]:
    """Query Conduit for all the transactions that ever happened to a Task."""
    for transaction in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            conduit_url, api_token, 'transaction.search', data={'objectIdentifier': str(task_phid)},
        ),
    ):
        transaction_type = assert_cast_optional(str, transaction.get('type'))

        kwargs = Transaction(
            id=assert_cast(int, transaction.get('id')),
            phid=assert_cast(str, transaction.get('phid')),
            task_phid=assert_cast(str, transaction.get('objectPHID')),
            author_phid=assert_cast(str, transaction.get('authorPHID')),
            date_created=datetime.datetime.fromtimestamp(
                assert_cast(int, transaction['dateCreated'])
            ),
            date_modified=(
                datetime.datetime.fromtimestamp(
                    assert_cast(int, transaction['dateModified']))
                if transaction.get('dateModified') is not None
                else None
            ),
        )

        if transaction_type == 'status':
            yield StatusTransaction(
                old=assert_cast_optional(str, fields(transaction).get('old')),
                new=assert_cast(str, fields(transaction).get('new')),
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'subtype':
            yield SubtypeTransaction(
                old=assert_cast_optional(str, fields(transaction).get('old')),
                new=assert_cast(str, fields(transaction).get('new')),
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'priority':
            yield PriorityTransaction(
                old=assert_cast_optional(int, assert_cast(
                    dict, fields(transaction).get('old')).get('value')),
                new=assert_cast(int, assert_cast(
                    dict, fields(transaction).get('new')).get('value')),
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'mergedinto' and 'new' in fields(transaction):
            yield MergedIntoTransaction(
                into_task_phid=assert_cast(
                    str, fields(transaction).get('new')),
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'owner':
            yield OwnerTransaction(
                old=assert_cast_optional(str, fields(transaction).get('old')),
                new=assert_cast_optional(str, fields(transaction).get('new')),
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'projects':
            yield ProjectsTransaction(
                operations=[
                    ProjectsTransactionOperation(
                        operation=assert_cast(str, assert_cast(
                            dict, operation).get('operation')),
                        phid=assert_cast(str, assert_cast(
                            dict, operation).get('phid')),
                    )
                    for operation in assert_cast(list, fields(transaction).get('operations'))
                ],
                **dc.asdict(kwargs),
            )


@dc.dataclass
class Moderator:
    phid: str


def fetch_moderators(
    conduit_url: yarl.URL,
    api_token: str,
    project_phid: str,
) -> List[Moderator]:
    """Query Conduit for all the members of a project

    This is used to filter out tasks created by "the community"
    and by the development team (volunteers and paid developers).

    Yields:
        Moderator objects.
    """

    response_data, cursor = result = fetch_single(
        conduit_url,
        api_token,
        'project.query',
        data={
            'phids[0]': str(project_phid)
        },
    )

    moderators = list()
    for moderator_phid in cast(
        Iterable[List[str]],
        response_data[project_phid]['members'],
    ):
        moderators.append(Moderator(phid=moderator_phid))

    return moderators


def store_moderators(
    moderators: Iterable[Moderator], connection: sqlite3.Connection
) -> None:
    """Save the latest moderators list, deleting all existing entries."""
    cursor: sqlite3.Cursor

    logger.info(f'Storing {len(moderators)} moderators.')

    with connection:
        for chunk in chunked(moderators, n=256):
            connection.executemany(
                '''
                INSERT INTO moderators (
                    phid
                )
                VALUES (
                    :phid
                )
                ''',
                [
                    {
                        'phid': moderator.phid,
                    }
                    for moderator in chunk
                ],
            )


def set_bf_blender(
        conduit_url: yarl.URL, api_token: str, blender_project_phid: str, connection: sqlite3.Connection):
    """
    Check if the untriaged tasks are still part of the BF-Blender project
    """
    cursor: sqlite3.Cursor

    # 1. Query all the existing untriaged queries in the blender project
    untriaged_tasks = list()
    for task in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            conduit_url,
            api_token,
            'maniphest.search',
            data={
                'constraints[projects][0]': str(blender_project_phid),
                'constraints[statuses][0]': 'needstriage',
                'constraints[subtypes][0]': 'default',  # report
                'order[0]': '-updated',
                'order[1]': '-id',
            },
        ),
    ):
        untriaged_tasks.append(task['id'])

    untriaged_tasks = set(untriaged_tasks)
    logger.info(
        f'There are {len(untriaged_tasks)} untriaged bf-blender in the server tasks.')

    # 2. Go over existing untriaged tasks and if they are not in the blender project
    # set them as bf-blender=0.
    # "select tasks.id from tasks WHERE tasks.subtype = "default" AND tasks.status = "needstriage";

    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        cursor.execute(
            '''
            SELECT tasks.id from tasks
            WHERE
            tasks.subtype = "default" AND
            tasks.status = "needstriage" AND
            bf_blender = 1;
            '''
        )
        result = cursor.fetchall()

    if not result:
        return None

    tasks_ids = [task_id for (task_id,) in result]
    logger.info(
        f'There are {len(tasks_ids)} untriaged tasks in the database.')

    with connection:
        for chunk in chunked(tasks_ids, n=256):
            connection.executemany(
                '''
                UPDATE tasks
                SET bf_blender = 0
                WHERE
                tasks.id = :task_id;
                ''',
                [
                    {
                        'task_id': task_id,
                    }
                    for task_id in chunk
                    if task_id not in untriaged_tasks
                ]
            )


def main() -> None:
    api_token = os.environ['CONDUIT_API_TOKEN']

    data_dir.mkdir(exist_ok=True)

    setup_logging(logger, 'log-task.txt')

    conduit_url = yarl.URL('https://developer.blender.org/api/')
    blender_project_phid = 'PHID-PROJ-hclk7tvd6pmpjmqastjl'
    moderators_project_phid = 'PHID-PROJ-2mzqsc6avm7hfpxuv7ub'

    with closing(sqlite3.connect(str(data_dir / 'tasks.sqlite'))) as connection:
        init_sqlite(connection)

        after: Optional[datetime.datetime]
        if len(sys.argv) > 1 and sys.argv[1]:
            after = datetime.datetime.fromisoformat(sys.argv[1])
        else:
            after = get_max_date_modified(connection)

        if after is not None:
            logger.info(f'Fetching updates after: {after.isoformat(sep=" ")}')

        for i, task in enumerate(
            fetch_tasks(conduit_url, api_token,
                        blender_project_phid, after=after)
        ):
            transactions = fetch_transactions(
                conduit_url, api_token, task_phid=task.phid)
            store_task_and_transactions(task, transactions, connection)

            if i % 10 == 0:
                current_after = task.date_modified

                if current_after is None:
                    logger.info(f'Fetched {i + 1} tasks.')
                else:
                    logger.info(
                        f'Fetched {i + 1} tasks. We now have all modifications after: {current_after.isoformat(sep=" ")}'
                    )

        # Get a new moderators list every time
        logger.info(f'Fetching moderators list')
        moderators = fetch_moderators(
            conduit_url, api_token, moderators_project_phid)
        store_moderators(moderators, connection)

        # Sanitize the reports to remove the ones that are no longer part of the Blender project
        set_bf_blender(
            conduit_url, api_token, blender_project_phid, connection)


if __name__ == '__main__':
    main()
