#!/usr/bin/env python3.8

from argparse import ArgumentParser
import dataclasses as dc
import datetime
import git
import logging
import sqlite3
import sys
import tempfile
import csv
import os
from contextlib import closing
from pathlib import Path
from typing import List, Optional
from enum import Enum

from .utils import assert_cast_optional, setup_logging, data_dir, T, blender_git_repository_get, get_query

logger = logging.getLogger(__file__)


class Mode(Enum):
    RESOLUTION = 'resolution'
    FIRST_RESPONSE = 'first_response'
    WEBSITE = 'website'


@dc.dataclass
class InputArguments:
    mode: Mode
    date_start: datetime.datetime
    date_end: datetime.datetime


def process_arguments() -> InputArguments:
    """Mode is mandatory, date range is optional."""
    parser = ArgumentParser(
        description='''
    Count tracker items for a range of days.
    By default will get today's data, for tasks that need developers attention.
    '''
    )
    parser.add_argument(
        'mode',
        type=Mode,
        choices=list(Mode),
        help='''
        mode parameters: resolution or first_response
        ''',
    )
    parser.add_argument(
        '--start',
        type=datetime.datetime.fromisoformat,
        help='''
        first day of range, today as default
        ''',
    )
    parser.add_argument(
        '--end',
        type=datetime.datetime.fromisoformat,
        help='''
        last day of range, optional, today as default
        ''',
    )

    arguments_raw = parser.parse_args()

    if arguments_raw.start is None:
        if arguments_raw.end is not None:
            parser.error(
                "The --end argument can only be used when --start is specified")
        else:
            arguments_raw.start = arguments_raw.end = datetime.datetime.today()

    elif arguments_raw.end is None:
        arguments_raw.end = datetime.datetime.today()

    elif arguments_raw.end < arguments_raw.start:
        parser.error("The --end argument needs to be later than --start")

    input_arguments = InputArguments(arguments_raw.mode, arguments_raw.start,
                                     arguments_raw.end)
    return input_arguments


def get_query_data(connection: sqlite3.Connection,
                   date_from: datetime.datetime,
                   date_to: datetime.datetime,
                   query_filename: str):
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query(query_filename)
        cursor.execute(query, {
            "DateFrom": date_from.strftime("%Y-%m-%d"),
            "DateTo": date_to.strftime("%Y-%m-%d"),
        })

        result = cursor.fetchall()
        return result


def from_sql_to_javascript(data, var_name: str):
    data_js = []
    for x, y in data:
        data_js.append({"x": x, "y": y})

    return "var " + var_name + " = " + \
        str(data_js) + ";"


def get_total(connection: sqlite3.Connection,
              date_from: datetime.datetime,
              date_to: datetime.datetime) -> int:
    return get_query_data(connection,
                          date_from,
                          date_to, "response_time_total.sql")[0][0]


def get_first_response(connection: sqlite3.Connection,
                       date_from: datetime.datetime,
                       date_to: datetime.datetime):
    logger.info("Calculating first response")
    data = get_query_data(connection, date_from, date_to,
                          "response_time_updated.sql")
    data_js = from_sql_to_javascript(data, "moderator_transaction_time_data")
    logger.info(data_js)
    return data_js


def get_resolution(connection: sqlite3.Connection,
                   date_from: datetime.datetime,
                   date_to: datetime.datetime):
    logger.info("Calculating resolution")
    data = get_query_data(connection, date_from,
                          date_to, "response_time_closed.sql")

    data_js = from_sql_to_javascript(data, "resolution_time_data")
    logger.info(data_js)
    return data_js


def generate_website(connection: sqlite3.Connection,
                     date_from: datetime.datetime,
                     date_to: datetime.datetime) -> None:
    template_dir = Path(__file__).parent.parent / 'templates'

    # get the data
    first_response = get_first_response(connection, date_from, date_to)
    resolution = get_resolution(connection, date_from, date_to)
    total = get_total(connection, date_from, date_to)

    # get the site
    template_filepath = template_dir / 'response-time.html'
    webpage = open(template_filepath, 'r').read()

    # process/replace the site
    webpage = webpage.replace("var taskCount = 4234;",
                              "var taskCount = {total};".format(total=total))

    webpage = webpage.replace(
        'var moderator_transaction_time_data = [{ "x": 0.003472222222222222, "y": 95.0 }, { "x": 0.010416666666666666, "y": 297.0 }, { "x": 0.020833333333333332, "y": 537.0 }, { "x": 0.041666666666666664, "y": 808.0 }, { "x": 0.08333333333333333, "y": 1130.0 }, { "x": 0.125, "y": 1334.0 }, { "x": 0.25, "y": 1729.0 }, { "x": 0.5, "y": 2182.0 }, { "x": 1.0, "y": 2884.0 }, { "x": 2.0, "y": 3291.0 }, { "x": 4.0, "y": 3581.0 }, { "x": 7.0, "y": 3716.0 }, { "x": 14.0, "y": 3844.0 }, { "x": 31.0, "y": 3953.0 }, { "x": 62.0, "y": 4006.0 }, { "x": 182.0, "y": 4086.0 }, { "x": 365.0, "y": 4086.0 }];',
        first_response)

    webpage = webpage.replace(
        'var resolution_time_data = [{ "x": 0.003472222222222222, "y": 18.0 }, { "x": 0.010416666666666666, "y": 51.0 }, { "x": 0.020833333333333332, "y": 107.0 }, { "x": 0.041666666666666664, "y": 185.0 }, { "x": 0.08333333333333333, "y": 300.0 }, { "x": 0.125, "y": 400.0 }, { "x": 0.25, "y": 570.0 }, { "x": 0.5, "y": 767.0 }, { "x": 1.0, "y": 1135.0 }, { "x": 2.0, "y": 1475.0 }, { "x": 4.0, "y": 1785.0 }, { "x": 7.0, "y": 2029.0 }, { "x": 14.0, "y": 2329.0 }, { "x": 31.0, "y": 2624.0 }, { "x": 62.0, "y": 2901.0 }, { "x": 182.0, "y": 3200.0 }, { "x": 365.0, "y": 3309.0 }];',
        resolution)

    # return/save the site
    new_filepath = template_dir / 'response-time-new.html'
    new_webpage = open(new_filepath, 'w')
    new_webpage.write(webpage)
    new_webpage.close()

    print("Website created on: " + str(new_filepath))


def main() -> None:
    setup_logging(logger, 'log-response-time.txt')
    arguments = process_arguments()

    with closing(sqlite3.connect(str(data_dir / 'tasks.sqlite'), uri=True)) as connection:
        from_date = arguments.date_start
        to_date = arguments.date_end

        if arguments.mode == Mode.FIRST_RESPONSE:
            get_first_response(
                connection, arguments.date_start, arguments.date_end)

        elif arguments.mode == Mode.RESOLUTION:
            get_resolution(
                connection, arguments.date_start, arguments.date_end)

        elif arguments.mode == Mode.WEBSITE:
            generate_website(connection, arguments.date_start,
                             arguments.date_end)


if __name__ == "__main__":
    main()
