#!/usr/bin/env python3.8

from argparse import ArgumentParser
import dataclasses as dc
import datetime
import git
import logging
import sqlite3
import sys
import tempfile
import csv
import os
from contextlib import closing
from pathlib import Path
from typing import List, Optional
from enum import Enum

from .utils import assert_cast_optional, setup_logging, data_dir, T, blender_git_repository_get, get_query

logger = logging.getLogger(__file__)

# The data was changed drastically on Christmas 2019 without even recorded transactions.
# So we need to handle the old queries differently than the new ones.
xmas = datetime.datetime(2019, 12, 23)

# In early 2014 the tracker migrated to phabricator. There are a lot of transaction missing for
# this period, so we treat it differently, relying mostly in the task latest status/priority.
sourceforge = datetime.datetime(2013, 11, 14)
@dc.dataclass
class InputArguments:
    query: Enum
    date_start: datetime.datetime
    date_end: datetime.datetime
    output: str
    git_repository: str


class Query(Enum):
    NEED_DEVELOPERS_ATTENTION = 'attention'
    OPEN_BUGS = 'bugs'
    NEW_REPORTS = 'new'
    UNTRIAGED = 'untriaged'
    OPEN = 'open'
    HIGH = 'high'
    HIGH_ALL = 'highall'
    NORMAL = 'normal'
    LOW = 'low'
    UNCLASSIFIED = 'unclassified'
    CONFIRMED = 'confirmed'
    KNOWN_ISSUES = 'knownissues'
    FIXED_BUGS = 'fixed'
    CLOSED = 'closed'
    COMMITTED_FIX = 'committedfix'
    COMMITTED_ANY = 'committedany'
    DEBUG_RESET = 'reset'

    def __str__(self):
        return self.value


@dc.dataclass
class Task:
    pass


@dc.dataclass
class Day:
    date: datetime.datetime
    query: Enum
    git_repository: Optional[str]
    total: int = 0

    def __str__(self):
        return "{date};{total};".format(date=self.date.strftime("%Y-%m-%d"), total=self.total)

    def __iter__(self):
        yield from (self.date.strftime("%Y-%m-%d"), self.total)

    def process(self, connection: sqlite3.Connection) -> None:
        if self.query == Query.NEED_DEVELOPERS_ATTENTION:
            self.total = get_need_developers_attention(connection, self.date)
        elif self.query == Query.OPEN_BUGS:
            self.total = get_open_bugs(connection, self.date)
        elif self.query == Query.NEW_REPORTS:
            self.total = get_new_reports(connection, self.date)
        elif self.query == Query.UNTRIAGED:
            self.total = get_untriaged(connection, self.date)
        elif self.query == Query.OPEN:
            self.total = get_open_tasks(connection, self.date)
        elif self.query == Query.CONFIRMED:
            self.total = get_confirmed(connection, self.date)
        elif self.query == Query.HIGH:
            self.total = get_high_priority_bugs(connection, self.date)
        elif self.query == Query.HIGH_ALL:
            self.total = get_high_priority_tasks(connection, self.date)
        elif self.query == Query.NORMAL:
            self.total = get_normal_priority_bugs(connection, self.date)
        elif self.query == Query.LOW:
            self.total = get_low_priority_bugs(connection, self.date)
        elif self.query == Query.UNCLASSIFIED:
            self.total = get_unclassified(connection, self.date)
        elif self.query == Query.KNOWN_ISSUES:
            self.total = get_known_issues(connection, self.date)
        elif self.query == Query.FIXED_BUGS:
            self.total = get_fixed_bugs(connection, self.date)
        elif self.query == Query.CLOSED:
            self.total = get_closed(connection, self.date)
        elif self.query == Query.COMMITTED_FIX:
            self.total = get_committed_fix(self.git_repository, self.date)
        elif self.query == Query.COMMITTED_ANY:
            self.total = get_committed_any(self.git_repository, self.date)
        else:
            logger.error("query type not fully implemented: %s" % (self.query,))


def process_arguments() -> InputArguments:
    """Mode is mandatory, date range is optional."""
    parser = ArgumentParser(
        description='''
    Count tracker items for a range of days.
    By default will get today's data, for tasks that need developers attention.
    '''
    )
    parser.add_argument(
        'query',
        type=Query,
        choices=list(Query),
        help='''
        query parameters: need developer attention (default), bugs, open reports, new reports or need triaging
        ''',
    )
    parser.add_argument(
        '--start',
        type=datetime.datetime.fromisoformat,
        help='''
        first day of range, today as default
        ''',
    )
    parser.add_argument(
        '--end',
        type=datetime.datetime.fromisoformat,
        help='''
        last day of range, optional, today as default
        ''',
    )
    parser.add_argument(
        '--output',
        type=str,
        default="",
        help='''
        optional output csv file
        ''',
    )
    parser.add_argument(
        '--git-repository',
        type=str,
        default="",
        help='''
        optional git repository for the 'committedfix' query
        ''',
    )

    arguments_raw = parser.parse_args()

    if arguments_raw.start is None:
        if arguments_raw.end is not None:
            parser.error("The --end argument can only be used when --start is specified")
        else:
            arguments_raw.start = arguments_raw.end = datetime.datetime.today()

    elif arguments_raw.end is None:
        arguments_raw.end = datetime.datetime.today()

    elif arguments_raw.end < arguments_raw.start:
        parser.error("The --end argument needs to be later than --start")

    if arguments_raw.query in (Query.COMMITTED_FIX, Query.COMMITTED_ANY) and \
        not arguments_raw.git_repository:
        parser.error("The '{query}' query requires a --git-repository argument".format(query=arguments_raw.query))

    input_arguments = InputArguments(arguments_raw.query, arguments_raw.start, arguments_raw.end, arguments_raw.output, arguments_raw.git_repository)
    return input_arguments


def output_filepath_get(filepath: str) -> str:
    """Return a valid filepath for the output csv file"""
    filepath = os.path.expanduser(filepath)

    if not filepath:
        output_file = tempfile.NamedTemporaryFile()
        return output_file.name

    if os.path.isdir(filepath):
        logger.error("The specified output is a folder, not a valid file: " + filepath)
        sys.exit(4)

    if os.path.isfile(filepath):
        if not os.access(filepath, os.W_OK):
            logger.error("Cannot write to " + filepath)
            sys.exit(3)
        return filepath

    dirname = os.path.dirname(filepath)
    if not os.path.isdir(dirname):
        logger.error("Filepath not in valid folder: " + dirname)
        sys.exit(6)

    if not os.access(dirname, os.W_OK):
        logger.error("Cannot create file in: " + dirname)
        sys.exit(3)

    return filepath


# ####################################################
# Handle queries
# ####################################################

def preprocessing_reset(connection: sqlite3.Connection) -> bool:
    """Delete the pre-processing tables"""
    connection.executescript('''
    DROP TABLE IF EXISTS "tasks_christmas_snapshot";
    DROP TABLE IF EXISTS "tasks_sourceforge_snapshot";
    ''')


def tasks_christmas_snapshot_exists(connection: sqlite3.Connection) -> bool:
    """Return if tasks_christmas_snapshot table exists"""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        cursor.execute('''
        SELECT count(name) FROM sqlite_master WHERE type='table' AND name='tasks_christmas_snapshot'
        ''')
        if cursor.fetchone()[0] == 1:
            return True
    return False


def tasks_christmas_snapshot_ensures(connection: sqlite3.Connection) -> bool:
    """Ensures the tasks_christmas_snapshot table exists and is populated, returns True if success"""
    cursor: sqlite3.Cursor

    connection.executescript('''
    DROP TABLE IF EXISTS "tasks_christmas_snapshot";
    
    CREATE TABLE "tasks_christmas_snapshot" (
        "task_id"    BIGINT NOT NULL,
        "status"    TEXT,
        "priority"    INT,
        "subtype"    TEXT,
        FOREIGN KEY("task_id") REFERENCES "tasks"("id")
    );
    ''')

    with closing(connection.cursor()) as cursor:
        query = get_query("xmas-preprocessing.sql")
        cursor.execute(query, {
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),
        })

    return tasks_christmas_snapshot_exists(connection)


def tasks_sourceforge_snapshot_exists(connection: sqlite3.Connection) -> bool:
    """Return if tasks_sourceforge_snapshot table exists"""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        cursor.execute('''
        SELECT count(name) FROM sqlite_master WHERE type='table' AND name='tasks_sourceforge_snapshot'
        ''')
        if cursor.fetchone()[0] == 1:
            return True
    return False


def tasks_sourceforge_ensures(connection: sqlite3.Connection) -> bool:
    """Ensures the tasks_sourceforge_snapshot table exists and is populated, returns True if success"""
    cursor: sqlite3.Cursor

    connection.executescript('''
    DROP TABLE IF EXISTS "tasks_sourceforge_snapshot";

    CREATE TABLE "tasks_sourceforge_snapshot" (
        "task_id"    BIGINT NOT NULL,
        "status"     TEXT,
        "priority"    INT,
        FOREIGN KEY("task_id") REFERENCES "tasks"("id")
    );
    ''')

    with closing(connection.cursor()) as cursor:
        query = get_query("sourceforge-refactor.sql")
        cursor.execute(query, {
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),
        })

    return tasks_sourceforge_snapshot_exists(connection)


def preprocessing_snapshot_ensures(connection: sqlite3.Connection) -> bool:
    """Creates the required pre-processing tables"""
    # For now we create them at all times.
    preprocessing_reset(connection)
    return tasks_sourceforge_ensures(connection) and tasks_christmas_snapshot_ensures(connection)


def get_open_bugs(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all the open (confirmed) bugs."""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": False,
            "SubtypeBug": True,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": False,
            "StatusNeedsTriage": False,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": True,
            "PriorityHigh": True,
            "PriorityNormal": True,
            "PriorityLow": True,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total


def get_need_developers_attention(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all the reports and bugs untriaged, needs developer to reproduce and confirmed."""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": True,
            "SubtypeBug": True,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": True,
            "StatusNeedsTriage": True,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": True,
            "PriorityHigh": True,
            "PriorityNormal": True,
            "PriorityLow": True,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total


def get_unclassified(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all the reports that are confirmed, but not yet a bug or known issue."""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": True,
            "SubtypeBug": False,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": False,
            "StatusNeedsTriage": False,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": True,
            "PriorityHigh": True,
            "PriorityNormal": True,
            "PriorityLow": True,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total


def get_confirmed(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all bugs and unclassified reports."""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": True,
            "SubtypeBug": True,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": False,
            "StatusNeedsTriage": False,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": True,
            "PriorityHigh": True,
            "PriorityNormal": True,
            "PriorityLow": True,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total


def get_high_priority_bugs(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all confirmed high priority and unbreak now bugs."""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": False,
            "SubtypeBug": True,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": False,
            "StatusNeedsTriage": False,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": True,
            "PriorityHigh": True,
            "PriorityNormal": False,
            "PriorityLow": False,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total


def get_high_priority_tasks(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all confirmed high priority and unbreak now tasks."""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": True,
            "SubtypeBug": True,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": False,
            "StatusNeedsTriage": False,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": True,
            "PriorityHigh": True,
            "PriorityNormal": False,
            "PriorityLow": False,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total


def get_low_priority_bugs(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all confirmed low priority bugs."""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": False,
            "SubtypeBug": True,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": False,
            "StatusNeedsTriage": False,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": False,
            "PriorityHigh": False,
            "PriorityNormal": False,
            "PriorityLow": True,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total


def get_normal_priority_bugs(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all confirmed normal priority bugs."""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": False,
            "SubtypeBug": True,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": False,
            "StatusNeedsTriage": False,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": False,
            "PriorityHigh": False,
            "PriorityNormal": True,
            "PriorityLow": False,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total


def get_untriaged(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all untriaged tasks."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": True,
            "SubtypeBug": True,
            "SubtypeKnownIssue": False,

            "StatusConfirmed": False,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": False,
            "StatusNeedsTriage": True,
            "StatusNeedsUserInfo": False,
            "StatusResolved": False,

            "PriorityUnbreakNow": True,
            "PriorityHigh": True,
            "PriorityNormal": True,
            "PriorityLow": True,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total
    return 0


def get_known_issues(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all open known issues."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
            "Sourceforge": sourceforge.strftime("%Y-%m-%d"),

            "SubtypeReport": False,
            "SubtypeBug": False,
            "SubtypeKnownIssue": True,

            "StatusConfirmed": True,
            "StatusDuplicate": False,
            "StatusInvalid": False,
            "StatusNeedsDeveloperToReproduce": True,
            "StatusNeedsTriage": True,
            "StatusNeedsUserInfo": True,
            "StatusResolved": True,

            "PriorityUnbreakNow": True,
            "PriorityHigh": True,
            "PriorityNormal": True,
            "PriorityLow": True,
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])

        if total is not None:
            return total
    return 0


def get_open_tasks(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all open tasks, that includes reports, bugs and known issues."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("open-tasks.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is not None:
            return total
    return 0


def get_committed_fix(git_repository: str, date: datetime.datetime) -> int:
    """Return all the reports of type bug closed as fixed."""
    import git
    repository = git.Git(git_repository)
    log_info = repository.log(
        '--before=' + (datetime.timedelta(days=1) + date).strftime("%Y-%m-%d") + ' 00:00',
        '--after=' + date.strftime("%Y-%m-%d") + ' 00:00',
        '--date=local',
        '--oneline',
        '--pretty=format:%s',
        '--no-merges',
    ).split('\n')

    return len([i for i in log_info if "fix" in i.lower()])


def get_committed_any(git_repository: str, date: datetime.datetime) -> int:
    """Return all the commits in a day."""
    import git
    repository = git.Git(git_repository)
    log_info = repository.log(
        '--before=' + (datetime.timedelta(days=1) + date).strftime("%Y-%m-%d") + ' 00:00',
        '--after=' + date.strftime("%Y-%m-%d") + ' 00:00',
        '--date=local',
        '--oneline',
        '--pretty=format:%s',
        '--no-merges',
    ).split('\n')

    return len(log_info)


def get_fixed_bugs(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all the reports of type bug closed as fixed."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("fixed-bugs.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Xmas": xmas.strftime("%Y-%m-%d"),
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is None:
            return 0
    return total


def get_closed(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all the reports closed."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("closed.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
        })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is None:
            return 0
    return total

def get_new_reports(connection: sqlite3.Connection, date: datetime.datetime) -> int:
    """Return all the new reports (be it report, known issue or bug)."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("new-reports.sql")
        cursor.execute(query, {"Date": date.strftime("%Y-%m-%d")})

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is None:
            return 0
    return total


def main() -> None:
    setup_logging(logger, 'log-task-plot.txt')
    arguments = process_arguments()
    csv_output = output_filepath_get(arguments.output)
    git_repository = blender_git_repository_get(arguments.git_repository)

    with closing(sqlite3.connect(str(data_dir / 'data.sqlite'))) as connection:

        if not preprocessing_snapshot_ensures(connection):
            logger.error("Could not created pre-processing tables")
            sys.exit(2)

        day = arguments.date_start
        last_day = arguments.date_end
        delta = datetime.timedelta(days=1)

        logger.debug("Output file: " + csv_output)
        open(csv_output, 'w', newline='').write("")

        while day <= last_day:
            today = Day(day, arguments.query, git_repository)

            logger.debug("Processing date: {date}".format(date=day.strftime("%Y-%m-%d")))

            # All the computation happens here
            today.process(connection)

            # Output, one open at a time to facilitate parse the output continuously
            with open(csv_output, 'a+', newline='') as csv_file:
                csv.writer(csv_file).writerow(today)
            logger.info(today)

            # Tomorrow
            day += delta


if __name__ == "__main__":
    main()
