WITH INPUT AS (SELECT
    :Date AS today,
    :Count AS count,
    :QueryId AS query)

REPLACE INTO "values" (id, date, count, query)
VALUES(
    (
        SELECT values_table.id FROM "values" AS values_table, INPUT
        WHERE INPUT.query=values_table.query AND date=INPUT.today
    ),
    (SELECT today FROM INPUT),
    (SELECT count FROM INPUT),
    (SELECT query FROM INPUT)
);
