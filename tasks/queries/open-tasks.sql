/*
SQL query to get all the tasks that were open at a point in time.

Note that there was a refactor around the christmas 2019 that changed
the default status of the new tasks (from 'open' to 'needstriage').

At that time, all the "open" tasks had the status 'open', later this became:
 * 'needs triage' (new default)
 * 'needs info from user'
 * 'needs developer to reproduce'
 * 'confirmed'

Expected input: Date (e.g., 2020-01-01), Xmas (2019, 12, 23)
*/

/*
-- left here for debugging
WITH constants AS (SELECT
    date("2019-08-07") AS today,
    date("2019-08-07", "+1 day") AS tomorrow,
    date("2019-12-23") AS xmas)
*/

WITH constants AS (SELECT
    date(:Date) AS today,
    date(:Date, "+1 day") AS tomorrow,
    date(:Xmas) AS xmas)


SELECT count(*) FROM (

SELECT
		constants.today AS today,
		tasks.id AS tasks_id,
		tasks.phid AS tasks_phid,
        (
        SELECT
                last_status_event.status
        FROM (
                /*
                Set an initial status value, and on top of it add all the
                status change transactions up to the specified date.
                We limit it to `1` to get only the latest status.
                */
                SELECT
                        tasks.date_created AS date,
                        CASE
                                WHEN   tasks.date_created < constants.xmas THEN 'open'
                                ELSE   'needstriage'
                        END            AS status,
                        2              AS _order
                        WHERE
                                tasks.date_created < constants.tomorrow
                UNION ALL
                SELECT
                        t.date_created AS date,
                        'duplicate'    AS status,
                        1              AS _order
                        FROM transactions_mergedinto tm
                        LEFT JOIN transactions t ON tm.transaction_id = t.id
                        WHERE
                                t.task_phid = tasks.phid
                                AND t.date_created < constants.tomorrow
                UNION ALL
                SELECT
                        t.date_created AS date,
                        ts.new         AS status,
                        0              AS _order
                        FROM transactions_status ts
                        LEFT JOIN transactions t ON ts.transaction_id = t.id
                        WHERE
                                t.task_phid = tasks.phid
                                AND t.date_created < constants.tomorrow
                ORDER BY
                        date DESC,
                        _order
                LIMIT 1
        ) last_status_event) AS status_at_point_in_time
FROM
        tasks,
        constants
WHERE
        tasks.date_created < constants.tomorrow
        AND (
                tasks.date_closed IS NULL OR
                tasks.date_closed >= constants.tomorrow
        )
        AND tasks.subtype IN (
                'default' /* aka report */,
                'knownissue',
                'bug'
                )
        /*
        We can't rely only on tasks.date_closed because the task may have been closed at the time,
        yet re-opened afterwards and then closed in the far future.
        */
        AND status_at_point_in_time IN (
                'open',
                'confirmed',
                'needstriage',
                'needsuserinfo',
                'needsdevelopertoreproduce'
        )
);