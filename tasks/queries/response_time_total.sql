WITH INPUT AS (SELECT
  strftime("%s", :DateFrom) as date_from,
  strftime("%s", :DateTo) as date_to
),
 RESPONSE_TIME AS
 (
SELECT
  CAST(strftime("%s", date_created) as INTEGER) as created
  FROM INPUT, tasks
  WHERE created > INPUT.date_from
  AND created <= INPUT.date_to
  AND subtype in ('bug', 'default', 'knownissue')
  AND tasks.status IN ('confirmed', 'resolved')
  AND tasks.author_phid NOT IN (SELECT phid FROM moderators)  
)
SELECT COUNT(*) FROM (SELECT * FROM RESPONSE_TIME)