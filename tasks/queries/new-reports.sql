SELECT count(*) FROM tasks
WHERE
    tasks.date_created >= :Date
    AND tasks.date_created < date(:Date, '+1 day')
    AND tasks.subtype IN ('default' /* aka report */, 'knownissue', 'bug');