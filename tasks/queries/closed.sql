SELECT count(*) FROM tasks
WHERE
    tasks.date_closed >= :Date
    AND tasks.date_closed < date(:Date, '+1 day')
;