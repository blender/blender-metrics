/*
SQL query to populate an secondary table to help the other queries.

There was a refactor around early 2014, and another on christmas 2019.

In the 2014 refactor not all transactions were ported, so we assume the latest
status and priority as the final one.

In the 2019 refactor the default status for new tasks changed, so we treat
each set of tasks separately.

Expected input: Date (e.g., 2020-01-01), Xmas (2019, 12, 23)

pre-refactor status
===================
* archived
* duplicate
* invalid
* open
* resolved

post-refactor status
====================
* confirmed
* invalid
* needsdevelopertoreproduce
* needstriage
* needsuserinfo
* resolved

*/

WITH constants AS (SELECT
    date(:Xmas) AS xmas,
    date(:Sourceforge) AS sourceforge
)

INSERT INTO tasks_christmas_snapshot (task_id, status, priority, subtype)

SELECT
    tasks_id AS task_id,

    /* The status with the refactored values
       For the actual refactor all the previously open
       that were not high priority were changed to
       needstriage.*/
    CASE
        WHEN
            priority_at_point_in_time IN (
                'needsdevelopertoreproduce',
                'needsuserinfo',
                'needstriage'
                )
             AND status_at_point_in_time = 'open'
        THEN priority_at_point_in_time
        WHEN
            status_at_point_in_time = 'open'
        THEN 'confirmed'
        ELSE status_at_point_in_time
    END AS status,

    /* The priority with the refactored values. */
    CASE
        WHEN
            priority_at_point_in_time IN (
                'needsdevelopertoreproduce',
                'needsuserinfo',
                'needstriage'
                )
        THEN 'normal'
        ELSE priority_at_point_in_time
    END AS priority,

    /* The subtype with the refactored values. */
    CASE
        WHEN priority_at_point_in_time IN ('high', 'unbreak') THEN 'bug'
        WHEN status_at_point_in_time IN ('resolved') THEN 'bug'
        ELSE 'default' -- report
    END AS subtype

    FROM
    (
        SELECT
            tasks.id AS tasks_id,
            tasks.phid AS tasks_phid,
            (
            SELECT
                CASE
                    /* The comments on each line are the new priority and status (for open tasks). */
                    WHEN last_priority_event.priority = 20 THEN 'low' -- low, confirmed
                    WHEN last_priority_event.priority = 40 THEN 'normal' -- normal, confirmed
                    WHEN last_priority_event.priority = 60 THEN 'high' -- high, confirmed
                    WHEN last_priority_event.priority = 100 THEN 'unbreak' -- unbreak, confirmed
                    WHEN last_priority_event.priority = 50 THEN 'normal' -- normal, confirmed
                    WHEN last_priority_event.priority = 80 THEN 'needsdevelopertoreproduce' -- normal, needsdevelopertoreproduce
                    WHEN last_priority_event.priority = 90 THEN 'needstriage' -- normal, needstriage
                    WHEN last_priority_event.priority = 30 THEN 'needsuserinfo' -- normal, needsuserinfo
                    ELSE 'other'
                END
            FROM
            (
                /*
                Set an initial status value, and on top of it add all the
                status change transactions up to the specified date.
                We limit it to `1` to get only the latest status.
                */
                SELECT
                    CASE
                        WHEN tasks.date_created < constants.sourceforge THEN constants.sourceforge
                        ELSE tasks.date_created
                    END        AS date,
                    CASE
                        WHEN tasks.date_created < constants.sourceforge THEN tasks_sourceforge_snapshot.priority
                        WHEN tasks.date_created < constants.xmas THEN 90 /* needs triage */
                        ELSE 40 /* normal */
                    END        AS priority,
                    2          AS _order
                    WHERE
                        tasks.date_created < constants.xmas
                UNION ALL
                SELECT
                    t.date_created AS date,
                    tp.new         AS priority,
                    0              AS _order
                    FROM transactions_priority tp
                    LEFT JOIN transactions t ON tp.transaction_id = t.id
                    WHERE
                        t.task_phid = tasks.phid
                        AND t.date_created < constants.xmas
                ORDER BY
                    date DESC,
                    _order
                LIMIT 1
            ) last_priority_event
        ) AS priority_at_point_in_time,
        (
            SELECT
                last_status_event.status
            FROM (
                /*
                Set an initial status value, and on top of it add all the
                status change transactions up to the specified date.
                We limit it to `1` to get only the latest status.
                */
                SELECT
                    CASE
                        WHEN tasks.date_created < constants.sourceforge THEN constants.sourceforge
                        ELSE tasks.date_created
                    END        AS date,
                    CASE
                        WHEN tasks.date_created < constants.sourceforge THEN tasks_sourceforge_snapshot.status
                        WHEN tasks.date_created < constants.xmas THEN 'open'
                        ELSE 'needstriage'
                    END        AS status,
                    2          AS _order
                    WHERE
                        tasks.date_created < constants.xmas
                UNION ALL
                SELECT
                    t.date_created AS date,
                    'duplicate'    AS status,
                    1              AS _order
                    FROM transactions_mergedinto tm
                    LEFT JOIN transactions t ON tm.transaction_id = t.id
                    WHERE
                        t.task_phid = tasks.phid
                        AND t.date_created < constants.xmas
                UNION ALL
                SELECT
                    t.date_created AS date,
                    ts.new         AS status,
                    0              AS _order
                    FROM transactions_status ts
                    LEFT JOIN transactions t ON ts.transaction_id = t.id
                    WHERE
                        t.task_phid = tasks.phid
                        AND t.date_created < constants.xmas
                ORDER BY
                    date DESC,
                    _order
                LIMIT 1
            ) last_status_event
        ) AS status_at_point_in_time
        FROM
            tasks LEFT JOIN tasks_sourceforge_snapshot ON tasks.id = tasks_sourceforge_snapshot.task_id,
            constants
        WHERE
            tasks.date_created < constants.xmas
            AND (
                tasks.date_closed IS NULL OR
                tasks.date_closed >= constants.xmas
          )
            AND tasks.subtype IN (
                'default' /* aka report */,
                'knownissue',
                'bug'
            )
    )
;
