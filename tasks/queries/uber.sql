WITH

/* -- for debugging
    INPUT AS ( SELECT
        date("2020-04-03") AS today,
        date("2020-04-03", "+1 day") AS tomorrow,
        date("2019-12-23") AS xmas,
        date("2013-11-14") AS sourceforge,
        0 AS report,
        1 AS bug,
        0 AS knownissue,
        1 AS confirmed,
        0 AS duplicate,
        0 AS invalid,
        0 AS needsdevelopertoreproduce,
        0 AS needstriage,
        0 AS needsuserinfo,
        0 AS resolved,
        1 AS unbreak,
        1 AS high,
        1 AS normal,
        1 AS low
    )
*/

    INPUT AS ( SELECT
        date(:Date) AS today,
        date(:Date, "+1 day") AS tomorrow,
        date(:Xmas) AS xmas,
        date(:Sourceforge) AS sourceforge,

    -- subtypes
        :SubtypeReport AS report,
        :SubtypeBug AS bug,
        :SubtypeKnownIssue AS knownissue,

        -- status
        :StatusConfirmed AS confirmed,
        :StatusDuplicate AS duplicate,
        :StatusInvalid AS invalid,
        :StatusNeedsDeveloperToReproduce AS needsdevelopertoreproduce,
        :StatusNeedsTriage AS needstriage,
        :StatusNeedsUserInfo AS needsuserinfo,
        :StatusResolved AS resolved,

        -- priority
        :PriorityUnbreakNow AS unbreak,
        :PriorityHigh AS high,
        :PriorityNormal AS normal,
        :PriorityLow AS low
    )

SELECT COUNT(*) FROM (

SELECT
    INPUT.today AS today,
    tasks_id,
    tasks_subtype,
    tasks_bf_blender,
    /*
    The following adjustments are only relevant when querying days before xmas.
    Because for these after that we have the status/priority_at_xmas.

    Also note that for the actual refactor all the previously open
    that were not high priority were changed to needstriage. So any
    graph crossing the xmas will see a spike in "Needs Triage" reports.

    There is no need to check for today < xmas because the conditions in both
    cases can only happen if the date is prior to xmas.
    */

    /*  The status with the refactored values. */
    CASE
        WHEN INPUT.today > INPUT.xmas THEN status_at_point_in_time
        WHEN
            priority_at_point_in_time IN (
                'needsdevelopertoreproduce',
                'needsuserinfo',
                'needstriage'
                )
            AND status_at_point_in_time = 'open'
        THEN priority_at_point_in_time
        WHEN
            status_at_point_in_time = 'open'
        THEN 'confirmed'
        ELSE status_at_point_in_time
    END AS status_,

    /* The priority with the refactored values. */
    CASE
        WHEN INPUT.today > INPUT.xmas THEN priority_at_point_in_time
        WHEN
            priority_at_point_in_time IN (
                'needsdevelopertoreproduce',
                'needsuserinfo',
                'needstriage',
                'confirmed'
                )
        THEN 'normal'
        ELSE priority_at_point_in_time
    END AS priority_,

    /* The subtype with the refactored values. */
    CASE
        WHEN INPUT.today > INPUT.xmas THEN subtype_at_point_in_time
        WHEN
            priority_at_point_in_time = 'confirmed'
        THEN 'bug'
        WHEN status_at_point_in_time = 'open' AND
             priority_at_point_in_time NOT IN (
                 'needsdevelopertoreproduce',
                 'needsuserinfo',
                 'needstriage'
                 )
        THEN 'bug'
        ELSE subtype_at_point_in_time
    END AS subtype_

    FROM
    (
        SELECT
        tasks.id AS tasks_id,
        tasks.subtype AS tasks_subtype,
        tasks.bf_blender AS tasks_bf_blender,
        (
            SELECT
                CASE
                    /* The comments on each line are the new priority and status (for open tasks). */
                    WHEN last_priority_event._priority = 20 THEN 'low' -- low, confirmed
                    WHEN last_priority_event._priority = 40 THEN 'normal' -- normal, confirmed
                    WHEN last_priority_event._priority = 60 THEN 'high' -- high, confirmed
                    WHEN last_priority_event._priority = 100 THEN 'unbreak' -- unbreak, confirmed
                    WHEN last_priority_event._priority = 50 THEN 'confirmed' -- normal, confirmed
                    WHEN last_priority_event._priority = 80 THEN 'needsdevelopertoreproduce' -- normal, needsdevelopertoreproduce
                    WHEN last_priority_event._priority = 90 THEN 'needstriage' -- normal, needstriage
                    WHEN last_priority_event._priority = 30 THEN 'needsuserinfo' -- normal, needsuserinfo
                    ELSE last_priority_event._priority -- last value is from xmas, so already a string
                END
            FROM
            (
                SELECT
                    CASE
                        WHEN tasks.date_created < INPUT.sourceforge THEN INPUT.sourceforge
                        ELSE tasks.date_created
                    END        AS date,
                    CASE
                        WHEN tasks.date_created < INPUT.sourceforge THEN tasks_sourceforge_snapshot.priority
                        WHEN tasks.date_created < INPUT.xmas THEN 90 /* needs triage */
                        ELSE 40 /* normal */
                    END        AS _priority,
                    3          AS _order
                    WHERE
                        tasks.date_created < INPUT.tomorrow
                UNION ALL
                SELECT
                    INPUT.xmas                        AS date,
                    tasks_christmas_snapshot.priority AS _priority,
                    2                                 AS _order
                    WHERE
                        tasks.date_created < INPUT.tomorrow
                        AND tasks.date_created < INPUT.xmas
                        AND INPUT.today >= INPUT.xmas
                        AND tasks_christmas_snapshot.priority IS NOT NULL
                UNION ALL
                SELECT
                    t.date_created AS date,
                    tp.new     AS _priority,
                    0          AS _order
                    FROM transactions_priority tp
                    LEFT JOIN transactions t ON tp.transaction_id = t.id
                    WHERE
                        t.task_phid = tasks.phid
                        AND t.date_created < INPUT.tomorrow
                ORDER BY
                    date DESC,
                    _order
                LIMIT 1
            ) last_priority_event
        ) AS priority_at_point_in_time,
        (
            SELECT
                last_status_event._status
            FROM
            (
                /*
                Set an initial status value, and on top of it add all the
                status change transactions up to the specified date.
                We limit it to `1` to get only the latest status.
                */
                SELECT
                    CASE
                        WHEN tasks.date_created < INPUT.sourceforge THEN INPUT.sourceforge
                        ELSE tasks.date_created
                    END            AS date,
                    CASE
                        WHEN tasks.date_created < INPUT.sourceforge THEN tasks_sourceforge_snapshot.status
                        WHEN tasks.date_created < INPUT.xmas THEN 'open'
                        ELSE 'needstriage'
                    END            AS _status,
                    3              AS _order
                    WHERE
                        tasks.date_created < INPUT.tomorrow
                UNION ALL
                SELECT
                    INPUT.xmas                      AS date,
                    tasks_christmas_snapshot.status AS _status,
                    2                               AS _order
                    WHERE
                        tasks.date_created < INPUT.tomorrow
                        AND tasks.date_created < INPUT.xmas
                        AND INPUT.today >= INPUT.xmas
                        AND tasks_christmas_snapshot.status IS NOT NULL
                UNION ALL
                SELECT
                    t.date_created AS date,
                    'duplicate'    AS _status,
                    1              AS _order
                    FROM transactions_mergedinto tm
                    LEFT JOIN transactions t ON tm.transaction_id = t.id
                    WHERE
                        t.task_phid = tasks.phid
                        AND t.date_created < INPUT.tomorrow
                UNION ALL
                SELECT
                    t.date_created AS date,
                    ts.new         AS _status,
                    0              AS _order
                    FROM transactions_status ts
                    LEFT JOIN transactions t ON ts.transaction_id = t.id
                    WHERE
                        t.task_phid = tasks.phid
                        AND t.date_created < INPUT.tomorrow
                ORDER BY
                    date DESC,
                    _order
                LIMIT 1
            ) last_status_event
        ) AS status_at_point_in_time,
        (
            SELECT
                last_subtype_event.subtype
            FROM
            (
                SELECT
                    CASE
                        WHEN   tasks.date_created < INPUT.sourceforge THEN INPUT.sourceforge
                        ELSE   tasks.date_created
                    END                    AS date,
                    'default' /* report */ AS subtype,
                    2                      AS _order
                    WHERE
                        tasks.date_created < INPUT.tomorrow
                UNION ALL
                SELECT
                    INPUT.xmas                       AS date,
                    tasks_christmas_snapshot.subtype AS _subtype,
                    1                                AS _order
                    WHERE
                        tasks.date_created < INPUT.tomorrow
                        AND tasks.date_created < INPUT.xmas
                        AND INPUT.today >= INPUT.xmas
                        AND tasks_christmas_snapshot.subtype IS NOT NULL
                UNION ALL
                SELECT
                    t.date_created AS date,
                    ts.new         AS subtype,
                    0              AS _order
                    FROM transactions_subtype ts
                    LEFT JOIN transactions t ON ts.transaction_id = t.id
                    WHERE
                        t.task_phid = tasks.phid
                        AND t.date_created < INPUT.tomorrow
                ORDER BY
                    date DESC,
                    _order
                LIMIT 1
            ) last_subtype_event
        ) AS subtype_at_point_in_time

        FROM
            tasks
                LEFT JOIN tasks_christmas_snapshot ON tasks.id = tasks_christmas_snapshot.task_id
                LEFT JOIN tasks_sourceforge_snapshot ON tasks.id = tasks_sourceforge_snapshot.task_id,
            INPUT
        WHERE
            tasks.date_created < INPUT.tomorrow
            AND (
                tasks.date_closed IS NULL OR
                tasks.date_closed >= INPUT.tomorrow
            )
    ),
    INPUT
WHERE
    -- exclude patches, todos and design tasks altogether
    tasks_subtype IN (
        'default' /* aka report */,
        'knownissue',
        'bug'
    )

    -- subtype
    AND ((subtype_ = 'bug' AND INPUT.bug)
    OR (subtype_ = 'default' AND INPUT.report)
    OR (subtype_ = 'knownissue' AND INPUT.knownissue))

    -- status
    AND ((status_ = 'confirmed' AND INPUT.confirmed)
    OR (status_ = 'duplicate' AND INPUT.duplicate)
    OR (status_ = 'invalid' AND INPUT.invalid)
    OR (status_ = 'needsdevelopertoreproduce' AND INPUT.needsdevelopertoreproduce)
    OR (status_ = 'needstriage' AND INPUT.needstriage)
    OR (status_ = 'needsuserinfo' AND INPUT.needsuserinfo)
    OR (status_ = 'resolved' AND INPUT.resolved))

    -- priority
    AND ((priority_ = 'unbreak' AND INPUT.unbreak)
    OR (priority_ = 'high' AND INPUT.high)
    OR (priority_ = 'normal' AND INPUT.normal)
    OR (priority_ = 'low' AND INPUT.low))

     -- still tagged as BF-Blender
     AND tasks_bf_blender = 1

) -- COUNT
;
