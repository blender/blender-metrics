WITH INPUT AS (SELECT
--  $__from / 1000  as date_from,
--  $__to / 1000 as date_to,
  strftime("%s", :DateFrom) as date_from,
  strftime("%s", :DateTo) as date_to,
  strftime("%s", "now") as now
),
MACROS AS (SELECT
  date_to - date_from as date_range FROM INPUT
),
BUCKETS AS (
  SELECT 5 * 60 AS m5,
  15 * 60 AS m15,
  30 * 60 AS m30,
  60 * 60 AS h1,
  2 * 60 * 60 AS h2,
  3 * 60 * 60 AS h3,
  6 * 60 * 60 AS h6,
  12 * 60 * 60 AS h12,
  24 * 60 * 60 AS d1,
  2 * 24 * 60 * 60 AS d2,
  4 * 24 * 60 * 60 AS d4,
  7 * 24 * 60 * 60 AS w1,
  14 * 24 * 60 * 60 AS w2,
  31 * 24 * 60 * 60 AS m1,
  62 * 24 * 60 * 60 AS m2,
  182 * 24 * 60 * 60 AS m6,
  365 * 24 * 60 * 60 AS y1
),
 RESPONSE_TIME AS
 (SELECT
  first_update - created AS delta_updated,
  closed - created AS delta_closed
  FROM
(
SELECT
  CAST(strftime("%s", date_created) as INTEGER) as created,
  (SELECT
    CASE
      WHEN date_closed is NULL THEN INPUT.now
      ELSE CAST(strftime("%s", date_closed) as INTEGER)
    END
  ) as closed,
  (
  SELECT CAST(strftime("%s", date_created) as INTEGER)
    FROM transactions
    WHERE transactions.task_phid = tasks.phid
    AND transactions.date_created > tasks.date_created
    AND transactions.author_phid != tasks.author_phid
    ORDER BY transactions.id
    LIMIT 1
) AS first_update
  FROM INPUT, tasks
  WHERE created > INPUT.date_from
  AND created <= INPUT.date_to
  AND subtype in ('bug', 'default', 'knownissue')
  AND tasks.status IN ('confirmed', 'resolved')
  AND tasks.author_phid NOT IN (SELECT phid FROM moderators)  
)
)
SELECT bucket_time / 86400.0, resolution FROM
(
SELECT BUCKETS.m5 AS bucket_time, COUNT(*) AS resolution FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < m5), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.m5 <= date_range
  UNION
SELECT BUCKETS.m15 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < m15), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.m15 <= date_range
  UNION
SELECT BUCKETS.m30 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < m30), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.m30 <= date_range
  UNION
SELECT BUCKETS.h1 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < h1), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.h1 <= date_range
  UNION
SELECT BUCKETS.h2 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < h2), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.h2 <= date_range
  UNION
SELECT BUCKETS.h3 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < h3), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.h3 <= date_range
  UNION
SELECT BUCKETS.h6 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < h6), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.h6 <= date_range
  UNION
SELECT BUCKETS.h12 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < h12), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.h12 <= date_range
  UNION
SELECT BUCKETS.d1 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < d1), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.d1 <= date_range
  UNION
SELECT BUCKETS.d2 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < d2), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.d2 <= date_range
  UNION
SELECT BUCKETS.d4 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < d4), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.d4 <= date_range
  UNION
SELECT BUCKETS.w1 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < w1), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.w1 <= date_range
  UNION
SELECT BUCKETS.w2 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < w2), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.w2 <= date_range
  UNION
SELECT BUCKETS.m1 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < m1), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.m1 <= date_range
  UNION
SELECT BUCKETS.m2 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < m2), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.m2 <= date_range
  UNION
SELECT BUCKETS.m6 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < m6), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.m6 <= date_range
  UNION
SELECT BUCKETS.y1 AS bucket_time, COUNT(*) AS resolution  FROM (SELECT * FROM RESPONSE_TIME, BUCKETS WHERE delta_closed < y1), BUCKETS, INPUT, MACROS
  WHERE BUCKETS.y1 <= date_range
)